# Kotlin - React Material Design Web Components
[![Kotlin](https://img.shields.io/badge/kotlin-1.4.0-orange.svg?logo=kotlin)](http://kotlinlang.org)

Kotlin React Material Design Web Components provide [Kotlin](https://kotlinlang.org/):two_hearts: bindings to [RMWC - React Material Web Components](https://github.com/jamesmfriedman/rmwc) :two_hearts:

[RMWC](https://github.com/jamesmfriedman/rmwc) is doing all of the hard work here

## Prerequisites

Before you begin, ensure you have met the following requirements:
* Your consuming project is  using Kotlin 1.4-M3
* You are not using the new Kotlin Javascript compiler backen

# How to use it

Select the project and its version and replace the
`{PROJECT-VERSION}` with the respective ones.

> It has not been build using the new Kotlin Javascript compiler backend ``` kotlin.js.compiler=ir ``` yet...
<!--
## Maven

**Adding the repository:**

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/20119500/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/20119500/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/20119500/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

**Adding the library:**

```xml
<dependency>
    <group>com.zsinz</group>
    <artifactId>kotlin-react-mdwc</artifactId>
    <version>{PROJECT-VERSION}</version>
</dependency>
```
-->

## Gradle

**Adding the repository:**
```kotlin
maven {
        url = uri( "https://gitlab.com/api/v4/groups/zsinz-kotlin/-/packages/maven" )
        name = "GitLab"
    }
```

**Adding the library:**

```kotlin
implementation("com.zsinz:kotlin-react-mdwc:{PROJECT-VERSION}")

implementation(npm("material-components-web", "3.2.0"))
implementation(npm("rmwc", "5.7.1"))
```

## Example of it use

* [Kotlin Multiplatform using Quarkus on the backend](https://gitlab.com/zsinz-kotlin/kotlin-multiplatform-quarkus)

## Contributing to Kotlin - React Material Design Web Components
<!--- If your README is long or you have some specific process or steps you want contributors to follow, consider creating a separate CONTRIBUTING.md file--->
To contribute to kotlin-react-mdwc, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the branch: `git push
5. Create a merge request

Alternatively see the GitLab documentation on [creating a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

## Contributors

Thanks to the following people who have contributed to this project:

* [@zsinz](https://gitlab.com/zsinz)

<!-- You might want to consider using something like the [All Contributors](https://github.com/all-contributors/all-contributors) specification and its [emoji key](https://allcontributors.org/docs/en/emoji-key). -->

## License
<!--- If you're not sure which open license to use see https://choosealicense.com/--->

This project uses the following license: [<license_name>](<link>).