plugins {
    kotlin("js") version "1.4.0"
    `maven-publish`
    signing
}
group = "com.zsinz"
version = "1.0.0-SNAPSHOTX"

val gitLabPrivateToken: String by project

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlin-js-wrappers")
    }

}
dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains:kotlin-react:16.13.1-pre.112-kotlin-1.4.0")
    implementation("org.jetbrains:kotlin-react-dom:16.13.1-pre.112-kotlin-1.4.0")
    implementation("org.jetbrains:kotlin-styled:1.0.0-pre.112-kotlin-1.4.0")

    implementation(npm("react","16.13.1"))
    implementation(npm("react-dom","16.13.1"))
    implementation(npm("react-is","16.13.1"))

    implementation(npm("styled-components","5.0.0"))
    implementation(npm("inline-style-prefixer","5.1.0"))

    implementation(npm("material-components-web", "3.2.0"))
    implementation(npm("rmwc", "5.7.1"))

//    testImplementation(kotlin("test-js"))
}
kotlin {
    js(IR) {
        useCommonJs()
        browser {
            testTask {
                useKarma {
                    useChromeHeadless()
                    webpackConfig.cssSupport.enabled = true
                }
            }
        }

        mavenPublication { // Setup the publication for the target 'jvm6'
            // The default artifactId was 'foo-jvm6', change it:
            artifactId = "${project.name}"
        }
    }

}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            artifactId = "${project.name}"
            from(components["kotlin"])

            pom {
                name.set("${project.name}")
                description.set("Kotlin bindings to the React Wrapper for Googles Material Design Web Components")
                licenses {
                    license {
                        name.set("The MIT License")
                        url.set("https://opensource.org/licenses/MITt")
                    }
                }
                developers {
                    developer {
                        id.set("darrenarbell")
                        name.set("Darren Bell")
                        email.set("darrenarbell@gmail.com")
                    }
                }
//                scm {
//                    connection.set("scm:git:git://example.com/my-library.git")
//                    developerConnection.set("scm:git:ssh://example.com/my-library.git")
//                    url.set("http://example.com/my-library/")
//                }
            }
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/20119500/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitLabPrivateToken
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

//signing {
//    sign(publishing.publications["mavenJava"])
//}

