package com.zsinz.kotlin.react.mdwc.builders

import org.w3c.dom.events.Event
import react.RBuilder
import com.zsinz.kotlin.react.mdwc.types.Button
import com.zsinz.kotlin.react.mdwc.types.IconButton

fun RBuilder.button(text: String, classes: dynamic = null, raised: Boolean? = null, isOutlined: Boolean?=null, onClick: (evt: Event)->Unit) = child(Button::class) {
  attrs {
    this.className = classes
    this.label = text
    this.raised = raised
    this.onClick = onClick
    this.outlined = isOutlined
  }
}



fun RBuilder.iconButton(icon: dynamic, classes: dynamic = null, onClick: (evt: Event)->Unit) = child(IconButton::class) {
  attrs {
    this.icon = icon
    this.className = classes
    this.onClick = onClick
  }
}
