package com.zsinz.kotlin.react.mdwc.builders

import org.w3c.dom.events.Event
import react.RBuilder
import react.RHandler
import react.RProps
import com.zsinz.kotlin.react.mdwc.types.*

fun RBuilder.card(
  style : dynamic = null,
  contents: RHandler<RProps>) = child(Card::class) {
  attrs {
  }
  contents()
}

fun RBuilder.cardPrimaryAction(
  contents: RHandler<RProps>) = child(CardPrimaryAction::class) {
  attrs {
  }
  contents()
}


fun RBuilder.cardMedia(
  isSixteenByNine : Boolean? = null,
  isSquare : Boolean? = null,
  style: dynamic? = null) = child(CardMedia::class) {
  attrs {
    this.sixteenByNine = isSixteenByNine
    this.square = isSquare
    this.style = style
  }
}


fun RBuilder.cardActions(
  isFullBleed : Boolean? = null,
  contents: RHandler<RProps>) = child(CardActions::class) {
  attrs {
    this.fullBleed = fullBleed
  }
  contents()
}


fun RBuilder.cardActionButtons(
  contents: RHandler<RProps>) = child(CardActionButtons::class) {
  attrs {
  }
  contents()
}

fun RBuilder.cardActionButton(
  text: String, raised: Boolean? = null, onClick: (evt: Event)->Unit) = child(CardActionButton::class) {
  attrs {
    this.label = text
    this.raised = raised
    this.onClick = onClick
  }
}

fun RBuilder.cardActionIcons(
  contents: RHandler<RProps>) = child(CardActionIcons::class) {
  attrs {
  }
  contents()
}


fun RBuilder.cardActionIcon(
  icon: dynamic, onIcon: dynamic? = null, onClick: (evt: Event)->Unit) = child(CardActionIcon::class) {
  attrs {
    this.icon = icon
    this.onIcon = onIcon
    this.onClick = onClick
  }
}
