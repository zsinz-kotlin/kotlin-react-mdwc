package com.zsinz.kotlin.react.mdwc.builders

import react.RBuilder
import react.RHandler
import react.RProps
import react.key
import com.zsinz.kotlin.react.mdwc.types.Chip
import com.zsinz.kotlin.react.mdwc.types.ChipInfo
import com.zsinz.kotlin.react.mdwc.types.ChipSet
import com.zsinz.kotlin.react.mdwc.types.CustomEvent

fun RBuilder.chipSet(key:dynamic? = null,  choice: Boolean?=false, filter: Boolean?=false, chips: RHandler<RProps>) = child(ChipSet::class) {
  attrs {
    this.key = key
    this.choice = choice
    this.filter = filter
  }
  chips()
}


fun RBuilder.chip(
  label: dynamic,
  selected: Boolean? = false,
  icon: dynamic = null,
  trailingIcon: dynamic = null,
  id: String? = null,
  checkmark: Boolean? = false,
  children: React.ReactNode? = null,
  onInteraction: ((evt: CustomEvent<ChipInfo>) -> Unit)? = null,
  onTrailingIconInteraction: ((evt: CustomEvent<ChipInfo>) -> Unit)? = null,
  onRemove: ((evt: CustomEvent<ChipInfo>) -> Unit)? = null

) = child(Chip::class) {
  attrs {
    this.label = label
    this.selected = selected
    this.icon = icon
    this.trailingIcon = trailingIcon
    this.id = id
    this.checkmark = checkmark
    this.children = children
    this.onInteraction = onInteraction
    this.onTrailingIconInteraction = onTrailingIconInteraction
    this.onRemove = onRemove
  }
}


