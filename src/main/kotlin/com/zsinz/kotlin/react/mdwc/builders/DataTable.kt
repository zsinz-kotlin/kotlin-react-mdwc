package com.zsinz.kotlin.react.mdwc.builders

import kotlinx.css.Color
import kotlinx.css.color
import org.w3c.dom.events.Event
import react.*
import react.dom.i
import com.zsinz.kotlin.react.mdwc.types.*
import styled.css
import styled.styledDiv
import styled.styledSpan

fun RBuilder.datatable(
  stickyRows: Number? = null,
  stickyColumns: Number? = null,
  handler: RHandler<DataTableProps>) = child(DataTable::class) {
  attrs {
    this.stickyRows = stickyRows
    this.stickyColumns = stickyColumns
  }
  handler()
}


fun RBuilder.datatableContent(
  handler: RHandler<DataTableContentProps>) = child(DataTableContent::class) {
  attrs {
  }
  handler()
}


fun RBuilder.datatableBody(
  handler: RHandler<DataTableBodyProps>) = child(DataTableBody::class) {
  attrs {
  }
  handler()
}


fun RBuilder.datatableHead(
  handler: RHandler<DataTableHeadProps>) = child(DataTableHead::class) {
  attrs {
  }
  handler()
}

fun RBuilder.datatableRow(
  key: String,
  selected: Boolean? = false,
  activated: Boolean? = false,
  handler: RHandler<DataTableRowProps>) = child(DataTableRow::class) {
  attrs {
    this.key = key
    this.selected = selected
    this.activated = activated
  }
  handler()
}

fun RBuilder.datatableHeadCell(
  className: String? = null,
  alignStart: Boolean? = null,
  alignMiddle: Boolean? = null,
  alignEnd: Boolean? = null,
  sort: Number? = undefined,
  onSortChange: ((dir: Number?) -> Unit)? = undefined,
  children: RHandler<RProps>) = child(DataTableHeadCell::class) {
  attrs {
    this.className = className
    this.alignStart = alignStart
    this.alignMiddle = alignMiddle
    this.alignEnd = alignEnd
    this.sort = sort
    this.onSortChange = onSortChange

  }
  children()
}

fun RBuilder.datatableCell(
  alignStart: Boolean? = null,
  alignMiddle: Boolean? = null,
  alignEnd: Boolean? = null,
  handler: RHandler<DataTableCellProps>) = child(DataTableCell::class) {
  attrs {
    this.alignStart = alignStart
    this.alignMiddle = alignMiddle
    this.alignEnd = alignEnd
  }
  handler()
}

//fun RBuilder.actionButton(icon: String, color: String, onClick: (event: Event) -> Unit) {
//  styledSpan {
//    css {
//      this.color = Color(color)
//    }
//    iconButton(icon = icon, onClick = onClick)
//  }
//}

fun RBuilder.editActionButton(onClick: (event: Event) -> Unit) {
  iconButton(classes="action-icon__edit", icon = "edit", onClick = onClick)
}

fun RBuilder.deleteActionButton(onClick: (event: Event) -> Unit) {
  iconButton(classes="action-icon__delete", icon = "delete_forever", onClick = onClick)
}

fun RBuilder.copyActionButton(onClick: (event: Event) -> Unit) {
  iconButton(classes = "action-icon__file_copy", icon = "file_copy", onClick = onClick)
}
