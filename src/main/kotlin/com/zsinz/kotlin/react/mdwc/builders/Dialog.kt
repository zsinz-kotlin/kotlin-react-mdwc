package com.zsinz.kotlin.react.mdwc.builders

import org.w3c.dom.events.Event
import react.RBuilder
import react.RHandler
import react.RProps
import com.zsinz.kotlin.react.mdwc.types.*

fun RBuilder.dialog(open: Boolean, onClose: ((evt: CustomEvent<DialogActionEvent>) -> Unit)? = null, children: RHandler<RProps>) = child(
  Dialog::class) {
  attrs {
    this.open = open
    this.onClose = onClose
  }
  children()
}

fun RBuilder.dialogTitle(children: RHandler<RProps>) = child(DialogTitle::class) {
  children()
}

fun RBuilder.dialogContent(children: RHandler<RProps>) = child(DialogContent::class) {
  children()
}

fun RBuilder.dialogActions(children: RHandler<RProps>) = child(DialogActions::class){
  children()
}

fun RBuilder.dialogButton(text:String, action:String, isDefaultAction:Boolean = false, isDisabled:Boolean = false, onClick: ((event: Event) -> Unit)? = null) = child(DialogButton::class) {
  attrs {
    this.label = text
    this.action = action
    this.isDefaultAction = isDefaultAction
    this.disabled = isDisabled
    this.onClick  = onClick
  }
}
