package com.zsinz.kotlin.react.mdwc.builders

import com.zsinz.kotlin.react.mdwc.types.*
import react.RBuilder
import react.RHandler
import react.RProps

fun RBuilder.drawer(classes: String?=null, open: Boolean? = null, modal: Boolean? = null, dismissible: Boolean? = null, onOpen: ((evt: CustomEvent<Any>) -> Unit)? = null,  onClose: ((evt: CustomEvent<Any>) -> Unit)? = null, children: RHandler<RProps>) = child(Drawer::class) {
  attrs {
    this.open = open
    this.modal = modal
    this.dismissible = dismissible
    this.onOpen = onOpen
    this.onClose = onClose
  }
  children()
}

fun RBuilder.drawerHeader(classes: String?=null, children: RHandler<RProps>) = child(DrawerHeader::class) {
  attrs {
    this.className= classes
  }
  children()
}

fun RBuilder.drawerTitle(classes: String?=null, children: RHandler<RProps>) = child(DrawerTitle::class) {
  attrs {
    this.className= classes
  }
  children()
}

fun RBuilder.drawerSubTitle(classes: String?=null, children: RHandler<RProps>) = child(DrawerSubtitle::class) {
  attrs {
    this.className= classes
  }
  children()
}

fun RBuilder.drawerContent(classes: String?=null, children: RHandler<RProps>) = child(DrawerContent::class){
  attrs {
    this.className= classes
  }
  children()
}
fun RBuilder.drawerAppContent(classes: String?=null, children: RHandler<RProps>) = child(DrawerAppContent::class){
  attrs {
    this.className= classes
  }
  children()
}

