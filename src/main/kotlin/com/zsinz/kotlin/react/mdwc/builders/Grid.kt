package com.zsinz.kotlin.react.mdwc.builders

import react.RBuilder
import react.RHandler
import com.zsinz.kotlin.react.mdwc.types.*

fun RBuilder.grid(classes: String?=null, handler: RHandler<GridProps>) = child(Grid::class) {
  attrs {
    this.className= classes
  }
  handler()
}

fun RBuilder.gridInner(classes: String?=null, handler: RHandler<GridInnerProps>) = child(GridInner::class) {
  attrs {
    this.className= classes
  }
  handler()
}

fun RBuilder.gridCell(classes: String?=null, span: Number? = null, handler: RHandler<GridCellProps>) = child(GridCell::class) {
  attrs {
    this.className= classes
    this.span = span
  }
  handler()
}
