package com.zsinz.kotlin.react.mdwc.builders

import React.ChangeEvent
import React.SyntheticEvent
import kotlinx.html.InputType
import org.w3c.dom.HTMLInputElement
import react.RBuilder
import react.RHandler
import react.RProps
import com.zsinz.kotlin.react.mdwc.types.SelectBase
import com.zsinz.kotlin.react.mdwc.types.TextField

data class FormattedOptionData(var label: String?, var value:Any)

//inline fun <T> jsApply(cb: T.() -> Unit): T {
//  val obj = js("{}")
//  cb(obj.unsafeCast<T>())
//  return obj.unsafeCast<T>()
//}

external interface SelectChange {
  var index: Number
  var value: String
}

fun RBuilder.select(
  classes: String = "",
  isRequired: Boolean? = null,
  label: String?,
  options: Array<Any>,
  initialValue: String? = null,
  handleChange: ((evt: ChangeEvent<SelectChange>)->Unit)? = null ) = child(SelectBase::class) {
  attrs {
    this.className= classes
    this.required = isRequired
    this.label = label
    this.options = options
    this.enhanced = true
    this.onChange = handleChange
    this.value = initialValue
  }
}

fun RBuilder.select(
  classes: String = "",
  isRequired: Boolean? = null,
  label: String?,
  initialValue: String? = null,
  handleChange: ((evt: ChangeEvent<SelectChange>)->Unit)?=null,
  children: RHandler<RProps>) = child(SelectBase::class) {
  attrs {
    this.className = classes
    this.required = isRequired
    this.label = label
    this.options = options
    this.enhanced = true
    this.onChange = handleChange
    this.value = initialValue
  }
  children()
}

fun RBuilder.inputField(
  label: String,
  classes: String = "",
  value: dynamic = "",
  pattern: String? = null,
  isRequired: Boolean = false,
  isTextArea: Boolean = false,
  isFullWidth: Boolean = false,
  isOutlined: Boolean = false,
  inputType: dynamic = InputType.text, //To overcome weird issue with the datetime-local control (It was using dateTimeLocal as the string)
  inputRef: dynamic = null,
  handleChange: (evt: SyntheticEvent<HTMLInputElement>)->Unit) = child(TextField::class) {

  attrs {
    this.className = classes
    this.label = label
    this.type = inputType
    this.value = value
    this.textarea = isTextArea
    this.required = isRequired
    this.fullwidth = isFullWidth
    this.outlined = isOutlined
    this.onChange = handleChange
    this.pattern = pattern
    if(inputRef!=null) {
      this.inputRef = inputRef
    }
  }
}

