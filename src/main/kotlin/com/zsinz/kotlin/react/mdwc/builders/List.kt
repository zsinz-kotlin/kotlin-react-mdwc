package com.zsinz.kotlin.react.mdwc.builders

import React.ChangeEvent
import react.RBuilder
import react.RHandler
import react.RProps
import com.zsinz.kotlin.react.mdwc.types.*
import react.key

fun RBuilder.list(
  onAction: ((evt: CustomEvent<Any>)->Unit)?=null , listItems: RHandler<ListProps>  ) = child(List::class) {
  attrs {
    this.onAction = onAction
  }

  listItems()
}



fun RBuilder.listItem(
  key: String,
  selected: Boolean? = false,
  activated: Boolean? = false,
  disabled: Boolean? = false,
  contents: RHandler<RProps>
   ) = child(ListItem::class) {
  attrs {
    this.key = key
    this.selected = selected
    this.activated = activated
    this.disabled = disabled
  }
  contents()
}

fun RBuilder.listItemGraphic(
  icon: dynamic?
) = child(ListItemGraphic::class) {
  attrs {
    this.icon = icon
  }
}


fun RBuilder.listItemText(
  contents: RHandler<RProps>
) = child(ListItemText::class) {
  attrs {
  }
  contents()
}

fun RBuilder.listItemPrimaryText(
  contents: RHandler<RProps>
) = child(ListItemPrimaryText::class) {
  attrs {
  }
  contents()
}


fun RBuilder.listItemSecondaryText(
  contents: RHandler<RProps>
) = child(ListItemSecondaryText::class) {
  attrs {
  }
  contents()
}

fun RBuilder.listItemMeta(
  icon: dynamic?,
  contents: RHandler<RProps>
) = child(ListItemMeta::class) {
  attrs {
    this.icon = icon
  }
  contents()
}
