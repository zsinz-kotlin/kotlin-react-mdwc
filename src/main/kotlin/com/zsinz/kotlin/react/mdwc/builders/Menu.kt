package com.zsinz.kotlin.react.mdwc.builders

import React.ChangeEvent
import org.w3c.dom.events.Event
import react.RBuilder
import react.RHandler
import react.RProps
import com.zsinz.kotlin.react.mdwc.types.*

fun RBuilder.menuSurfaceAnchor(
  contents: RHandler<RProps>) = child(MenuSurfaceAnchor::class) {
  attrs {
  }
  contents()
}

fun RBuilder.menuSurface(
  contents: RHandler<RProps>) = child(MenuSurface::class) {
  attrs {
  }
  contents()
}


fun RBuilder.menu(
  open: Boolean? = false,
  onOpen: ((evt: CustomEvent<Any>) -> Unit)? = null,
  onClose: ((evt: CustomEvent<Any>) -> Unit)? = null,
  onSelect: ((evt: CustomEvent<Any>) -> Unit)?=null,
  contents: RHandler<RProps>) = child(Menu::class) {
  attrs {
    this.open = open
    this.onSelect = onSelect
    this.onClose = onClose
    this.onOpen = onOpen
  }
  contents()
}


fun RBuilder.menuItem(
  onAction: ((evt: CustomEvent<Any>)->Unit)?=null,
  contents: RHandler<RProps>) = child(MenuItem::class) {
  attrs {
    this.onAction = onAction
  }
  contents()
}


