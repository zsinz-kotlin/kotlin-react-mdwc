package com.zsinz.kotlin.react.mdwc.builders

import com.zsinz.kotlin.react.mdwc.types.*
import org.w3c.dom.events.Event
import react.RBuilder
import react.RHandler
import react.RProps

fun RBuilder.snackbar(open: Boolean, onClose: ((evt: CustomEvent<SnackbarOnCloseEvent>) -> Unit)? = null, message: String?, dismissesOnAction: Boolean?, action: dynamic) = child(
        Snackbar::class) {
  attrs {
    this.open = open
    this.onClose = onClose
    this.message = message
    this.dismissesOnAction = dismissesOnAction
    this.action = action
  }
}