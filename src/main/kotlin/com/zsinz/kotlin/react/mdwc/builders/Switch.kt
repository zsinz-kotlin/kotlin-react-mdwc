package com.zsinz.kotlin.react.mdwc.builders

import com.zsinz.kotlin.react.mdwc.types.Switch
import org.w3c.dom.events.Event
import react.RBuilder

fun RBuilder.toggle(classes: String = "",label: String, checked: dynamic, onClick: (evt: Event)->Unit) = child(com.zsinz.kotlin.react.mdwc.types.Switch::class) {
  attrs {
    this.className = classes
    this.label = label
    this.checked = checked
    this.onClick = onClick
  }
}
