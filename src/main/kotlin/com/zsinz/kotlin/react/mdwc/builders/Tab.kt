package com.zsinz.kotlin.react.mdwc.builders

import react.RBuilder
import react.RHandler
import react.RProps
import com.zsinz.kotlin.react.mdwc.types.CustomEvent
import com.zsinz.kotlin.react.mdwc.types.Tab
import com.zsinz.kotlin.react.mdwc.types.TabBar
import com.zsinz.kotlin.react.mdwc.types.TabBarState

fun RBuilder.tab(tabName: String, isActive: Boolean = false) = child(Tab::class) {
  attrs {
    this.label = tabName
  }
}

fun RBuilder.tabBar(activeTabIndex: Number? = null, onActivate: ((evt: CustomEvent<TabBarState>) -> Unit)? = null, children: RHandler<RProps>) = child(
  TabBar::class) {
  attrs {
    this.activeTabIndex = activeTabIndex
    this.onActivate = onActivate
  }
  children()
}
