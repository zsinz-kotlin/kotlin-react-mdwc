package com.zsinz.kotlin.react.mdwc.builders

import com.zsinz.kotlin.react.mdwc.types.*
import org.w3c.dom.Element
import react.RBuilder
import react.RHandler

fun RBuilder.topAppBar(classes: String?=null, fixed: Boolean?=null, prominent: Boolean?=null, short: Boolean?=null, shortCollapsed: Boolean?=null, dense: Boolean?=null, scrollTarget: Element?=null, onNav: ((evt: CustomEvent<Any>) -> Unit)?=null, handler: RHandler<TopAppBarProps>) = child(TopAppBar::class) {
  attrs {
    this.className= classes
    this.fixed = fixed
    this.prominent = prominent
    this.short = short
    this.shortCollapsed = shortCollapsed
    this.dense = dense
    this.scrollTarget = scrollTarget
    this.onNav = onNav
  }
  handler()
}


fun RBuilder.topAppBarRow(classes: String?=null, handler: RHandler<TopAppBarRowProps>) = child(TopAppBarRow::class) {
  attrs {
    this.className= classes
  }
  handler()
}

fun RBuilder.topAppBarSection(classes: String?=null, alignStart: Boolean?=null, alignEnd: Boolean?=null, handler: RHandler<TopAppBarSectionProps>) = child(TopAppBarSection::class) {
  attrs {
    this.className= classes
    this.alignStart=alignStart
    this.alignEnd=alignEnd
  }
  handler()
}

fun RBuilder.topAppBarNavigationIcon(classes: String?=null, handler: RHandler<TopAppBarNavigationIconProps>) = child(
  TopAppBarNavigationIcon::class) {
  attrs {
    this.className= classes
  }
  handler()
}

fun RBuilder.topAppBarActionItem(classes: String?=null, handler: RHandler<TopAppBarActionItemProps>) = child(TopAppBarActionItem::class) {
  attrs {
    this.className= classes
  }
  handler()
}

fun RBuilder.topAppBarTitle(classes: String?=null, handler: RHandler<TopAppBarTitleProps>) = child(TopAppBarTitle::class) {
  attrs {
    this.className= classes
  }
  handler()
}

fun RBuilder.topAppBarFixedAdjust(classes: String?=null, dense: Boolean?=null, prominent: Boolean?=null, denseProminent: Boolean?=null, short: Boolean?=null, handler: RHandler<TopAppBarFixedAdjustProps>) = child(TopAppBarFixedAdjust::class) {
  attrs {
    this.className= classes
    this.dense=dense
    this.prominent=prominent
    this.denseProminent=denseProminent
    this.short=short
  }
  handler()
}

