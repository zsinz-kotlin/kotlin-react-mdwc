package com.zsinz.kotlin.react.mdwc.builders

import kotlinx.css.CSSBuilder
import kotlinx.css.RuleSet
import kotlinx.html.Tag
import react.RBuilder
import react.RHandler
import react.RProps
import com.zsinz.kotlin.react.mdwc.types.CardActionButtons
import com.zsinz.kotlin.react.mdwc.types.Typography

fun RBuilder.typography(
  tag: dynamic,
  use: dynamic,
  theme: dynamic? = null,
  style : dynamic? = null,
  contents: RHandler<RProps>) = child(Typography::class) {
  attrs {
    this.use = use
    this.tag = tag
    this.theme = theme
    this. style = style
  }
  contents()
}


//var Tag.style: RuleSet
//  get() = error("style cannot be read from props")
//  set(value) = jsStyle {
//    CSSBuilder().apply(value).declarations.forEach {
//      this[it.key] = when (it.value) {
//        !is String, !is Number -> it.value.toString()
//        else -> it.value
//      }
//    }
//  }
//
//fun Tag.style(handler: RuleSet) {
//  style = handler
//}
