@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*

typealias EventType = Any

typealias SpecificEventListener<K> = (evt: Any) -> Unit

typealias CustomEventListener<E> = (evt: E) -> Unit

external interface CssClassMap {
}

@Suppress("NOTHING_TO_INLINE")
inline operator fun CssClassMap.get(key: String): String? = asDynamic()[key]

@Suppress("NOTHING_TO_INLINE")
inline operator fun CssClassMap.set(key: String, value: String) {
  asDynamic()[key] = value
}

external interface SemanticStringContants {
}

@Suppress("NOTHING_TO_INLINE")
inline operator fun SemanticStringContants.get(key: String): String? = asDynamic()[key]

@Suppress("NOTHING_TO_INLINE")
inline operator fun SemanticStringContants.set(key: String, value: String) {
  asDynamic()[key] = value
}

external interface SemanticNumberConstants {
}

@Suppress("NOTHING_TO_INLINE")
inline operator fun SemanticNumberConstants.get(key: String): Number? = asDynamic()[key]

@Suppress("NOTHING_TO_INLINE")
inline operator fun SemanticNumberConstants.set(key: String, value: Number) {
  asDynamic()[key] = value
}

external open class MDCFoundation<AdapterType : Any>(adapter: AdapterType? = definedExternally) {
  open var adapter_: AdapterType
  open fun init()
  open fun destroy()

  companion object {
    var cssClasses: CssClassMap
    var strings: SemanticStringContants
    var numbers: SemanticNumberConstants
    var defaultAdapter: Any
  }
}
