@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/list")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
//import org.w3c.dom.*
import react.Component
import react.RProps
import react.RState
import com.zsinz.kotlin.react.mdwc.types.*

external interface ListItemProps : WithRippleProps {
  var selected: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var activated: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var disabled: Boolean?
    get() = definedExternally
    set(value) = definedExternally
}

@JsName("ListItem")
external open class ListItem(props: ListItemProps) : Component<ListItemProps, RState> {
  override fun render(): dynamic
}

//external var ListItem: ListItemProps // React.ComponentType<ListItemProps /* ListItemProps & Pick<RMWC.ComponentProps, dynamic /* "onError" | "className" | "theme" | "ref" | "cite" | "data" | "form" | "label" | "span" | "style" | "summary" | "title" | "pattern" | "children" | "onBlur" | "onClick" | "onContextMenu" | "onCopy" | "onCut" | "onAuxClick" | "onDoubleClick" | "onDragEnd" | "onDragStart" | "onDrop" | "onFocus" | "onInput" | "onInvalid" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onMouseDown" | "onMouseUp" | "onPaste" | "onPause" | "onPlay" | "onPointerCancel" | "onPointerDown" | "onPointerUp" | "onRateChange" | "onReset" | "onSeeked" | "onSubmit" | "onTouchCancel" | "onTouchEnd" | "onTouchStart" | "onVolumeChange" | "onAbort" | "onAnimationEnd" | "onAnimationIteration" | "onAnimationStart" | "onCanPlay" | "onCanPlayThrough" | "onDrag" | "onDragEnter" | "onDragExit" | "onDragLeave" | "onDragOver" | "onDurationChange" | "onEmptied" | "onEncrypted" | "onEnded" | "onGotPointerCapture" | "onLoad" | "onLoadedData" | "onLoadedMetadata" | "onLoadStart" | "onLostPointerCapture" | "onMouseMove" | "onMouseOut" | "onMouseOver" | "onPlaying" | "onPointerMove" | "onPointerOut" | "onPointerOver" | "onProgress" | "onScroll" | "onSeeking" | "onStalled" | "onSuspend" | "onTimeUpdate" | "onTouchMove" | "onTransitionEnd" | "onWaiting" | "onWheel" | "onMouseEnter" | "onMouseLeave" | "onPointerEnter" | "onPointerLeave" | "onChange" | "onSelect" | "onBeforeInput" | "onCompositionEnd" | "onCompositionStart" | "onCompositionUpdate" | "key" | "tag" | "accept" | "acceptCharset" | "action" | "allowFullScreen" | "allowTransparency" | "alt" | "as" | "async" | "autoComplete" | "autoFocus" | "autoPlay" | "capture" | "cellPadding" | "cellSpacing" | "charSet" | "challenge" | "checked" | "classID" | "cols" | "colSpan" | "content" | "controls" | "coords" | "crossOrigin" | "dateTime" | "default" | "defer" | "download" | "encType" | "formAction" | "formEncType" | "formMethod" | "formNoValidate" | "formTarget" | "frameBorder" | "headers" | "height" | "high" | "href" | "hrefLang" | "htmlFor" | "httpEquiv" | "integrity" | "keyParams" | "keyType" | "kind" | "list" | "loop" | "low" | "manifest" | "marginHeight" | "marginWidth" | "max" | "maxLength" | "media" | "mediaGroup" | "method" | "min" | "minLength" | "multiple" | "muted" | "name" | "nonce" | "noValidate" | "open" | "optimum" | "placeholder" | "playsInline" | "poster" | "preload" | "readOnly" | "rel" | "required" | "reversed" | "rows" | "rowSpan" | "sandbox" | "scope" | "scoped" | "scrolling" | "seamless" | "shape" | "size" | "sizes" | "src" | "srcDoc" | "srcLang" | "srcSet" | "start" | "step" | "target" | "type" | "useMap" | "value" | "width" | "wmode" | "wrap" | "defaultChecked" | "defaultValue" | "suppressContentEditableWarning" | "suppressHydrationWarning" | "accessKey" | "contentEditable" | "contextMenu" | "dir" | "draggable" | "hidden" | "id" | "lang" | "slot" | "spellCheck" | "tabIndex" | "inputMode" | "is" | "radioGroup" | "role" | "about" | "datatype" | "inlist" | "prefix" | "property" | "resource" | "typeof" | "vocab" | "autoCapitalize" | "autoCorrect" | "autoSave" | "color" | "itemProp" | "itemScope" | "itemType" | "itemID" | "itemRef" | "results" | "security" | "unselectable" | "aria-activedescendant" | "aria-atomic" | "aria-autocomplete" | "aria-busy" | "aria-checked" | "aria-colcount" | "aria-colindex" | "aria-colspan" | "aria-controls" | "aria-current" | "aria-describedby" | "aria-details" | "aria-disabled" | "aria-dropeffect" | "aria-errormessage" | "aria-expanded" | "aria-flowto" | "aria-grabbed" | "aria-haspopup" | "aria-hidden" | "aria-invalid" | "aria-keyshortcuts" | "aria-label" | "aria-labelledby" | "aria-level" | "aria-live" | "aria-modal" | "aria-multiline" | "aria-multiselectable" | "aria-orientation" | "aria-owns" | "aria-placeholder" | "aria-posinset" | "aria-pressed" | "aria-readonly" | "aria-relevant" | "aria-required" | "aria-roledescription" | "aria-rowcount" | "aria-rowindex" | "aria-rowspan" | "aria-selected" | "aria-setsize" | "aria-sort" | "aria-valuemax" | "aria-valuemin" | "aria-valuenow" | "aria-valuetext" | "dangerouslySetInnerHTML" | "onCopyCapture" | "onCutCapture" | "onPasteCapture" | "onCompositionEndCapture" | "onCompositionStartCapture" | "onCompositionUpdateCapture" | "onFocusCapture" | "onBlurCapture" | "onChangeCapture" | "onBeforeInputCapture" | "onInputCapture" | "onResetCapture" | "onSubmitCapture" | "onInvalidCapture" | "onLoadCapture" | "onErrorCapture" | "onKeyDownCapture" | "onKeyPressCapture" | "onKeyUpCapture" | "onAbortCapture" | "onCanPlayCapture" | "onCanPlayThroughCapture" | "onDurationChangeCapture" | "onEmptiedCapture" | "onEncryptedCapture" | "onEndedCapture" | "onLoadedDataCapture" | "onLoadedMetadataCapture" | "onLoadStartCapture" | "onPauseCapture" | "onPlayCapture" | "onPlayingCapture" | "onProgressCapture" | "onRateChangeCapture" | "onSeekedCapture" | "onSeekingCapture" | "onStalledCapture" | "onSuspendCapture" | "onTimeUpdateCapture" | "onVolumeChangeCapture" | "onWaitingCapture" | "onAuxClickCapture" | "onClickCapture" | "onContextMenuCapture" | "onDoubleClickCapture" | "onDragCapture" | "onDragEndCapture" | "onDragEnterCapture" | "onDragExitCapture" | "onDragLeaveCapture" | "onDragOverCapture" | "onDragStartCapture" | "onDropCapture" | "onMouseDownCapture" | "onMouseMoveCapture" | "onMouseOutCapture" | "onMouseOverCapture" | "onMouseUpCapture" | "onSelectCapture" | "onTouchCancelCapture" | "onTouchEndCapture" | "onTouchMoveCapture" | "onTouchStartCapture" | "onPointerDownCapture" | "onPointerMoveCapture" | "onPointerUpCapture" | "onPointerCancelCapture" | "onPointerEnterCapture" | "onPointerLeaveCapture" | "onPointerOverCapture" | "onPointerOutCapture" | "onGotPointerCaptureCapture" | "onLostPointerCaptureCapture" | "onScrollCapture" | "onWheelCapture" | "onAnimationStartCapture" | "onAnimationEndCapture" | "onAnimationIterationCapture" | "onTransitionEndCapture" */> & RMWC.WithRippleProps */>

external interface ListItemTextProps: RProps

//external var ListItemText: ListItemTextProps //React.ComponentType<ListItemTextProps /* ListItemTextProps & Any */>
@JsName("ListItemText")
external open class ListItemText(props: ListItemTextProps) : Component<ListItemTextProps, RState> {
  override fun render(): dynamic
}


external interface ListItemPrimaryTextProps: RProps

//external var ListItemPrimaryText: ListItemPrimaryTextProps//React.ComponentType<ListItemPrimaryTextProps /* ListItemPrimaryTextProps & Any */>
@JsName("ListItemPrimaryText")
external open class ListItemPrimaryText(props: ListItemPrimaryTextProps) : Component<ListItemPrimaryTextProps, RState> {
  override fun render(): dynamic
}

external interface ListItemSecondaryTextProps: RProps

//external var ListItemSecondaryText: ListItemSecondaryTextProps //React.ComponentType<ListItemSecondaryTextProps /* ListItemSecondaryTextProps & Any */>
@JsName("ListItemSecondaryText")
external open class ListItemSecondaryText(props: ListItemSecondaryTextProps) : Component<ListItemSecondaryTextProps, RState> {
  override fun render(): dynamic
}

external interface ListItemGraphicProps : IconProps

//external var ListItemGraphic: ListItemGraphicProps //React.ComponentType<ListItemGraphicProps /* ListItemGraphicProps & Any */>
@JsName("ListItemGraphic")
external open class ListItemGraphic(props: ListItemGraphicProps) : Component<ListItemGraphicProps, RState> {
  override fun render(): dynamic
}


external interface ListItemMetaProps : IconProps

//external var ListItemMeta: ListItemMetaProps //React.ComponentType<ListItemMetaProps /* ListItemMetaProps & Any */>
@JsName("ListItemMeta")
external open class ListItemMeta(props: ListItemMetaProps) : Component<ListItemMetaProps, RState> {
  override fun render(): dynamic
}

external interface ListGroupProps: RProps

//external var ListGroup: ListGroupProps //React.ComponentType<ListGroupProps /* ListGroupProps & Any */>
@JsName("ListGroup")
external open class ListGroup(props: ListGroupProps) : Component<ListGroupProps, RState> {
  override fun render(): dynamic
}

external interface ListGroupSubheaderProps : RProps

//external var ListGroupSubheader: ListGroupSubheaderProps //React.ComponentType<ListGroupSubheaderProps /* ListGroupSubheaderProps & Any */>
@JsName("ListGroupSubheader")
external open class ListGroupSubheader(props: ListGroupSubheaderProps) : Component<ListGroupSubheaderProps, RState> {
  override fun render(): dynamic
}

external interface ListDividerProps: RProps

//external var ListDivider: ListDividerProps //React.ComponentType<ListDividerProps /* ListDividerProps & Any */>
@JsName("ListDivider")
external open class ListDivider(props: ListDividerProps) : Component<ListDividerProps, RState> {
  override fun render(): dynamic
}

external interface SimpleListItemProps : ListItemProps {
  var text: React.ReactNode?
    get() = definedExternally
    set(value) = definedExternally
  var secondaryText: React.ReactNode?
    get() = definedExternally
    set(value) = definedExternally
  var graphic: dynamic /* IconElementT | IconOptions */
    get() = definedExternally
    set(value) = definedExternally
  var metaIcon: dynamic /* IconElementT | IconOptions */
    get() = definedExternally
    set(value) = definedExternally
  var meta: React.ReactNode?
    get() = definedExternally
    set(value) = definedExternally
  var children: React.ReactNode?
    get() = definedExternally
    set(value) = definedExternally
}

//external object SimpleListItem {
//    @nativeInvoke
//    operator fun invoke(__0: SimpleListItemProps /* SimpleListItemProps & RMWC.ComponentProps */): JSX.Element
//    var displayName: String
//}



external interface ListProps : ComponentProps {
    var dense: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var twoLine: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var avatarList: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var nonInteractive: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var onAction: ((evt: CustomEvent<Any>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
}

//external interface `T$0` {
//    var LIST_ITEM_ACTIVATED_CLASS: String
//    var LIST_ITEM_CLASS: String
//    var LIST_ITEM_DISABLED_CLASS: String
//    var LIST_ITEM_SELECTED_CLASS: String
//    var ROOT: String
//}


external interface MDCListAdapter {
}

external open class MDCListFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) :
  MDCFoundation<MDCListAdapter> {
}


external open class List(props: ListProps) : FoundationComponent<MDCListFoundation, ListProps, RState> {
//    open var root: Any
//    open var listElements: Array<HTMLLIElement>
//    open fun componentDidMount()
//    open fun focusItemAtIndex(index: Number)
//    open fun getDefaultFoundation(): MDCListFoundation
//    open fun getListItemIndex(evt: FocusEvent): Number
//    open fun getListItemIndex(evt: KeyboardEvent): Number
//    open fun getListItemIndex(evt: MouseEvent): Number
//    open fun handleClick(evt: MouseEvent)
//    open fun handleKeydown(evt: KeyboardEvent /* React.KeyboardEvent<HTMLElement> & KeyboardEvent */)
//    open fun handleFocusIn(evt: FocusEvent /* React.FocusEvent & FocusEvent */)
//    open fun handleFocusOut(evt: FocusEvent /* React.FocusEvent & FocusEvent */)
//    open fun render(): JSX.Element
//
//    companion object {
//        var cssClasses: `T$0`
//    }
}
