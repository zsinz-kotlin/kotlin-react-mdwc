@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsModule("react")
package React

import org.w3c.dom.events.*
import org.w3c.dom.*
import org.w3c.dom.DragEvent

import org.w3c.dom.events.CompositionEvent
import org.w3c.dom.events.FocusEvent
import org.w3c.dom.events.KeyboardEvent
import org.w3c.dom.events.UIEvent
import org.w3c.dom.events.WheelEvent
import org.w3c.dom.svg.SVGElement



external interface ReactNode
external interface AbstractView {
  //    var styleMedia: StyleMedia
  var document: Document
}
external interface Touch

external interface TouchList {
//  @nativeGetter
//  operator fun get(index: Number): Touch?
//  @nativeSetter
//  operator fun set(index: Number, value: Touch)
  var length: Number
  fun item(index: Number): Touch
  fun identifiedTouch(identifier: Number): Touch
}

external interface SyntheticEvent<T> {
  var bubbles: Boolean
  var currentTarget: T /* EventTarget & T */
  var cancelable: Boolean
  var defaultPrevented: Boolean
  var eventPhase: Number
  var isTrusted: Boolean
  val nativeEvent: Event
  fun preventDefault()
  fun isDefaultPrevented(): Boolean
  fun stopPropagation()
  fun isPropagationStopped(): Boolean
  fun persist()
  var target: T
  var timeStamp: Number
  var type: String
}

external interface ChangeEvent<T> : SyntheticEvent<T> {
  override var target: T /* EventTarget & T */
}

external interface ClipboardEvent<T> : SyntheticEvent<T> {
  var clipboardData: DataTransfer
  override val nativeEvent: Event
//    override val nativeEvent: ClipboardEvent
}
external interface CompositionEvent<T> : SyntheticEvent<T> {
  var data: String
  override val nativeEvent: CompositionEvent
}
external interface DragEvent<T> : MouseEvent<T> {
  var dataTransfer: DataTransfer
  override val nativeEvent: DragEvent
}
external interface FocusEvent<T> : SyntheticEvent<T> {
  override val nativeEvent: FocusEvent
  var relatedTarget: EventTarget
}
external interface FormEvent<T> : SyntheticEvent<T>
external interface InvalidEvent<T> : SyntheticEvent<T> {
  override var target: T /* EventTarget & T */
}

external interface KeyboardEvent<T> : SyntheticEvent<T> {
  var altKey: Boolean
  var charCode: Number
  var ctrlKey: Boolean
  fun getModifierState(key: String): Boolean
  var key: String
  var keyCode: Number
  var locale: String
  var location: Number
  var metaKey: Boolean
  override val nativeEvent: KeyboardEvent
  var repeat: Boolean
  var shiftKey: Boolean
  var which: Number
}
external interface MouseEvent<T> : SyntheticEvent<T> {
  var altKey: Boolean
  var button: Number
  var buttons: Number
  var clientX: Number
  var clientY: Number
  var ctrlKey: Boolean
  fun getModifierState(key: String): Boolean
  var metaKey: Boolean
  override val nativeEvent: org.w3c.dom.events.MouseEvent
  var pageX: Number
  var pageY: Number
  var relatedTarget: EventTarget
  var screenX: Number
  var screenY: Number
  var shiftKey: Boolean
}
external interface TouchEvent<T> : SyntheticEvent<T> {
  var altKey: Boolean
  var changedTouches: TouchList
  var ctrlKey: Boolean
  fun getModifierState(key: String): Boolean
  var metaKey: Boolean
  //    override val nativeEvent: TouchEvent
  override val nativeEvent: Event
  var shiftKey: Boolean
  var targetTouches: TouchList
  var touches: TouchList
}
external interface UIEvent<T> : SyntheticEvent<T> {
  var detail: Number
  override val nativeEvent: UIEvent
  var view: AbstractView
}
external interface WheelEvent<T> : MouseEvent<T> {
  var deltaMode: Number
  var deltaX: Number
  var deltaY: Number
  var deltaZ: Number
  override val nativeEvent: WheelEvent
}
external interface AnimationEvent<T> : SyntheticEvent<T> {
  var animationName: String
  var elapsedTime: Number
  //    override val nativeEvent: AnimationEvent
  override val nativeEvent: Event
  var pseudoElement: String
}
external interface TransitionEvent<T> : SyntheticEvent<T> {
  var elapsedTime: Number
  //    override val nativeEvent: TransitionEvent
  override val nativeEvent: Event
  var propertyName: String
  var pseudoElement: String
}
