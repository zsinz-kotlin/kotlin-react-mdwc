@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/button")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RComponent
import react.RState

external interface ButtonProps : WithRippleProps {
    var dense: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var raised: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var unelevated: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var outlined: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var disabled: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var danger: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var label: dynamic /* React.ReactNode | Any */
        get() = definedExternally
        set(value) = definedExternally
    var children: React.ReactNode?
        get() = definedExternally
        set(value) = definedExternally
    var icon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var trailingIcon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("Button")
external open class Button: Component<ButtonProps, RState> {
  override fun render()
}

external interface ButtonIconProps : IconProps

external var ButtonIcon: Component<ButtonIconProps /* ButtonIconProps & Any */, RState>
