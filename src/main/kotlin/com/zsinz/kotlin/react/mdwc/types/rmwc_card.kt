@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")

@file:JsModule("@rmwc/card")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RState
import com.zsinz.kotlin.react.mdwc.types.*

external interface CardProps : ComponentProps {
    var outlined: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}


@JsName("Card")
external open class Card(props: CardProps) :
  Component<CardProps, RState> {
  override fun render(): dynamic
}

external interface CardMediaProps : ComponentProps{
    var square: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var sixteenByNine: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

//external var CardMedia: React.ComponentType<CardMediaProps /* CardMediaProps & Any */>
@JsName("CardMedia")
external open class CardMedia(props: CardMediaProps) :
  Component<CardMediaProps, RState> {
  override fun render(): dynamic
}

external interface CardMediaContentProps : ComponentProps

//external var CardMediaContent: React.ComponentType<CardMediaContentProps /* CardMediaContentProps & Any */>
@JsName("CardMediaContent")
external open class CardMediaContent(props: CardMediaContentProps) :
  Component<CardMediaContentProps, RState> {
  override fun render(): dynamic
}


external interface CardPrimaryActionProps : ComponentProps

//external var CardPrimaryAction: React.ComponentType<CardPrimaryActionProps /* CardPrimaryActionProps & Pick<RMWC.ComponentProps, dynamic /* "onError" | "className" | "theme" | "ref" | "cite" | "data" | "form" | "label" | "span" | "style" | "summary" | "title" | "pattern" | "children" | "onBlur" | "onClick" | "onContextMenu" | "onCopy" | "onCut" | "onAuxClick" | "onDoubleClick" | "onDragEnd" | "onDragStart" | "onDrop" | "onFocus" | "onInput" | "onInvalid" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onMouseDown" | "onMouseUp" | "onPaste" | "onPause" | "onPlay" | "onPointerCancel" | "onPointerDown" | "onPointerUp" | "onRateChange" | "onReset" | "onSeeked" | "onSubmit" | "onTouchCancel" | "onTouchEnd" | "onTouchStart" | "onVolumeChange" | "onAbort" | "onAnimationEnd" | "onAnimationIteration" | "onAnimationStart" | "onCanPlay" | "onCanPlayThrough" | "onDrag" | "onDragEnter" | "onDragExit" | "onDragLeave" | "onDragOver" | "onDurationChange" | "onEmptied" | "onEncrypted" | "onEnded" | "onGotPointerCapture" | "onLoad" | "onLoadedData" | "onLoadedMetadata" | "onLoadStart" | "onLostPointerCapture" | "onMouseMove" | "onMouseOut" | "onMouseOver" | "onPlaying" | "onPointerMove" | "onPointerOut" | "onPointerOver" | "onProgress" | "onScroll" | "onSeeking" | "onStalled" | "onSuspend" | "onTimeUpdate" | "onTouchMove" | "onTransitionEnd" | "onWaiting" | "onWheel" | "onMouseEnter" | "onMouseLeave" | "onPointerEnter" | "onPointerLeave" | "onChange" | "onSelect" | "onBeforeInput" | "onCompositionEnd" | "onCompositionStart" | "onCompositionUpdate" | "key" | "tag" | "accept" | "acceptCharset" | "action" | "allowFullScreen" | "allowTransparency" | "alt" | "as" | "async" | "autoComplete" | "autoFocus" | "autoPlay" | "capture" | "cellPadding" | "cellSpacing" | "charSet" | "challenge" | "checked" | "classID" | "cols" | "colSpan" | "content" | "controls" | "coords" | "crossOrigin" | "dateTime" | "default" | "defer" | "disabled" | "download" | "encType" | "formAction" | "formEncType" | "formMethod" | "formNoValidate" | "formTarget" | "frameBorder" | "headers" | "height" | "high" | "href" | "hrefLang" | "htmlFor" | "httpEquiv" | "integrity" | "keyParams" | "keyType" | "kind" | "list" | "loop" | "low" | "manifest" | "marginHeight" | "marginWidth" | "max" | "maxLength" | "media" | "mediaGroup" | "method" | "min" | "minLength" | "multiple" | "muted" | "name" | "nonce" | "noValidate" | "open" | "optimum" | "placeholder" | "playsInline" | "poster" | "preload" | "readOnly" | "rel" | "required" | "reversed" | "rows" | "rowSpan" | "sandbox" | "scope" | "scoped" | "scrolling" | "seamless" | "selected" | "shape" | "size" | "sizes" | "src" | "srcDoc" | "srcLang" | "srcSet" | "start" | "step" | "target" | "type" | "useMap" | "value" | "width" | "wmode" | "wrap" | "defaultChecked" | "defaultValue" | "suppressContentEditableWarning" | "suppressHydrationWarning" | "accessKey" | "contentEditable" | "contextMenu" | "dir" | "draggable" | "hidden" | "id" | "lang" | "slot" | "spellCheck" | "tabIndex" | "inputMode" | "is" | "radioGroup" | "role" | "about" | "datatype" | "inlist" | "prefix" | "property" | "resource" | "typeof" | "vocab" | "autoCapitalize" | "autoCorrect" | "autoSave" | "color" | "itemProp" | "itemScope" | "itemType" | "itemID" | "itemRef" | "results" | "security" | "unselectable" | "aria-activedescendant" | "aria-atomic" | "aria-autocomplete" | "aria-busy" | "aria-checked" | "aria-colcount" | "aria-colindex" | "aria-colspan" | "aria-controls" | "aria-current" | "aria-describedby" | "aria-details" | "aria-disabled" | "aria-dropeffect" | "aria-errormessage" | "aria-expanded" | "aria-flowto" | "aria-grabbed" | "aria-haspopup" | "aria-hidden" | "aria-invalid" | "aria-keyshortcuts" | "aria-label" | "aria-labelledby" | "aria-level" | "aria-live" | "aria-modal" | "aria-multiline" | "aria-multiselectable" | "aria-orientation" | "aria-owns" | "aria-placeholder" | "aria-posinset" | "aria-pressed" | "aria-readonly" | "aria-relevant" | "aria-required" | "aria-roledescription" | "aria-rowcount" | "aria-rowindex" | "aria-rowspan" | "aria-selected" | "aria-setsize" | "aria-sort" | "aria-valuemax" | "aria-valuemin" | "aria-valuenow" | "aria-valuetext" | "dangerouslySetInnerHTML" | "onCopyCapture" | "onCutCapture" | "onPasteCapture" | "onCompositionEndCapture" | "onCompositionStartCapture" | "onCompositionUpdateCapture" | "onFocusCapture" | "onBlurCapture" | "onChangeCapture" | "onBeforeInputCapture" | "onInputCapture" | "onResetCapture" | "onSubmitCapture" | "onInvalidCapture" | "onLoadCapture" | "onErrorCapture" | "onKeyDownCapture" | "onKeyPressCapture" | "onKeyUpCapture" | "onAbortCapture" | "onCanPlayCapture" | "onCanPlayThroughCapture" | "onDurationChangeCapture" | "onEmptiedCapture" | "onEncryptedCapture" | "onEndedCapture" | "onLoadedDataCapture" | "onLoadedMetadataCapture" | "onLoadStartCapture" | "onPauseCapture" | "onPlayCapture" | "onPlayingCapture" | "onProgressCapture" | "onRateChangeCapture" | "onSeekedCapture" | "onSeekingCapture" | "onStalledCapture" | "onSuspendCapture" | "onTimeUpdateCapture" | "onVolumeChangeCapture" | "onWaitingCapture" | "onAuxClickCapture" | "onClickCapture" | "onContextMenuCapture" | "onDoubleClickCapture" | "onDragCapture" | "onDragEndCapture" | "onDragEnterCapture" | "onDragExitCapture" | "onDragLeaveCapture" | "onDragOverCapture" | "onDragStartCapture" | "onDropCapture" | "onMouseDownCapture" | "onMouseMoveCapture" | "onMouseOutCapture" | "onMouseOverCapture" | "onMouseUpCapture" | "onSelectCapture" | "onTouchCancelCapture" | "onTouchEndCapture" | "onTouchMoveCapture" | "onTouchStartCapture" | "onPointerDownCapture" | "onPointerMoveCapture" | "onPointerUpCapture" | "onPointerCancelCapture" | "onPointerEnterCapture" | "onPointerLeaveCapture" | "onPointerOverCapture" | "onPointerOutCapture" | "onGotPointerCaptureCapture" | "onLostPointerCaptureCapture" | "onScrollCapture" | "onWheelCapture" | "onAnimationStartCapture" | "onAnimationEndCapture" | "onAnimationIterationCapture" | "onTransitionEndCapture" */> & RMWC.WithRippleProps */>
@JsName("CardPrimaryAction")
external open class CardPrimaryAction(props: CardPrimaryActionProps) :
  Component<CardPrimaryActionProps, RState> {
  override fun render(): dynamic
}


external interface CardActionsProps : ComponentProps{
    var fullBleed: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

external interface CardActionButtonsProps : WithRippleProps


//external var CardActions: React.ComponentType<CardActionsProps /* CardActionsProps & Any */>
@JsName("CardActions")
external open class CardActions(props: CardActionsProps) :
  Component<CardActionsProps, RState> {
  override fun render(): dynamic
}

//external var CardActionButtons: React.ComponentType<CardActionButtonsProps /* CardActionButtonsProps & Any */>
@JsName("CardActionButtons")
external open class CardActionButtons(props: CardActionButtonsProps) :
  Component<CardActionButtonsProps, RState> {
  override fun render(): dynamic
}

external interface CardActionIconsProps : ComponentProps

//external var CardActionIcons: React.ComponentType<CardActionIconsProps /* CardActionIconsProps & Any */>
@JsName("CardActionIcons")
external open class CardActionIcons(props: CardActionIconsProps) :
  Component<CardActionIconsProps, RState> {
  override fun render(): dynamic
}


external interface CardActionIconProps : IconButtonProps

//external var CardActionIcon: React.ComponentType<CardActionIconProps /* CardActionIconProps & Any */>
@JsName("CardActionIcon")
external open class CardActionIcon(props: CardActionIconProps) :
  Component<CardActionIconProps, RState> {
  override fun render(): dynamic
}

external interface CardActionButtonProps : ButtonProps

//external var CardActionButton: React.ComponentType<CardActionButtonProps /* CardActionButtonProps & Any */>
@JsName("CardActionButton")
external open class CardActionButton(props: CardActionButtonProps) :
  Component<CardActionButtonProps, RState> {
  override fun render(): dynamic
}


//external object CardAction {
//    @nativeInvoke
//    operator fun invoke(props: IconButtonProps /* IconButtonProps & ButtonProps */): JSX.Element
//    var displayName: String
//}

@JsName("CardAction")
external open class CardAction(props: IconButtonProps) :
  Component<IconButtonProps, RState> {
  override fun render(): dynamic
}

