@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/chip")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RState

external interface ChipInfo {
    var chipId: String
}

//typealias ChipOnInteractionEventT = CustomEvent<ChipInfo>
//
//typealias ChipOnTrailingIconInteractionEventT = CustomEvent<ChipInfo>
//
//typealias ChipOnRemoveEventT = CustomEvent<ChipInfo>

external interface ChipProps : WithRippleProps{
    var label: React.ReactNode?
        get() = definedExternally
        set(value) = definedExternally
    var selected: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var icon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var trailingIcon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var id: String?
        get() = definedExternally
        set(value) = definedExternally
    var checkmark: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var children: React.ReactNode?
        get() = definedExternally
        set(value) = definedExternally
    var onInteraction: ((evt: CustomEvent<ChipInfo>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var onTrailingIconInteraction: ((evt: CustomEvent<ChipInfo>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var onRemove: ((evt: CustomEvent<ChipInfo>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
}

external interface DeprecatedChipProps {
    var text: React.ReactNode?
        get() = definedExternally
        set(value) = definedExternally
}

external interface MDCChipAdapter {
}

external open class MDCChipFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) :
  MDCFoundation<MDCMenuSurfaceAdapter> {
}


@JsName("Chip")
external open class Chip(props: ChipProps) : FoundationComponent<MDCChipFoundation, ChipProps /* ChipProps & DeprecatedChipProps */, RState> {
//    open var root: Any
//    open var id: String
//    open var checkmarkEl: HTMLDivElement?
//    open var _reactInternalFiber: Any
//    open fun componentDidMount()
//    open fun getDefaultFoundation(): MDCChipFoundation
//    open fun handleInteraction(evt: React.MouseEvent /* React.MouseEvent & React.KeyboardEvent & MouseEvent & KeyboardEvent */)
//    open fun handleTransitionEnd(evt: React.TransitionEvent /* React.TransitionEvent & TransitionEvent */)
//    open fun handleTrailingIconInteraction(evt: Any)
//    open fun render(): JSX.Element
//
//    companion object {
//        var displayName: String
//    }
}

external interface ChipIconProps : IconProps {
    var leading: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var trailing: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

//external object ChipIcon {
//  @nativeInvoke
//  operator fun invoke(props: ChipIconProps /* ChipIconProps & RMWC.ComponentProps */): JSX.Element
//  var displayName: String
//}

@JsName("ChipIcon")
external open class ChipIcon(props: ChipIconProps) :
  Component<MenuItemsProps, RState> {
  override fun render(): dynamic
}


external interface ChipSetProps : WithRippleProps{
    var choice: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var filter: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

//external var ChipSet: React.ComponentType<ChipSetProps /* ChipSetProps & Any */>
@JsName("ChipSet")
external open class ChipSet(props: ChipSetProps) :
  Component<ChipSetProps, RState> {
  override fun render(): dynamic
}
