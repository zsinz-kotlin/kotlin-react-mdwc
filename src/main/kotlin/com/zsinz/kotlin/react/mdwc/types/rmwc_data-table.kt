@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/data-table")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RProps
import react.RState

external interface SharedDataTableCellProps : ComponentProps {
  var alignStart: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var alignMiddle: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var alignEnd: Boolean?
    get() = definedExternally
    set(value) = definedExternally
}

external interface DataTableProps : ComponentProps {
  var stickyRows: Number?
    get() = definedExternally
    set(value) = definedExternally
  var stickyColumns: Number?
    get() = definedExternally
    set(value) = definedExternally
}

//external var DataTable: Component<DataTableProps, RState>
external class DataTable: Component<DataTableProps, RState> {
  override fun render()
}

external interface DataTableContentProps:  RProps

//external var DataTableContent: Component<DataTableContentProps, RState>
external class DataTableContent: Component<DataTableContentProps, RState> {
  override fun render()
}

external interface DataTableHeadProps:  RProps

//external var DataTableHead: Component<DataTableHeadProps, RState>
external class DataTableHead: Component<DataTableHeadProps, RState> {
  override fun render()
}

external interface DataTableBodyProps:  RProps

//external var DataTableBody: Component<DataTableBodyProps, RState>
external class DataTableBody: Component<DataTableBodyProps, RState> {
  override fun render()
}

external interface DataTableRowProps:  RProps {
  var selected: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var activated: Boolean?
    get() = definedExternally
    set(value) = definedExternally
}

//external var DataTableRow: Component<DataTableRowProps, RState>
external class DataTableRow: Component<DataTableRowProps, RState> {
  override fun render()
}

external interface DataTableHeadCellProps : SharedDataTableCellProps {
  var sort: Number?
    get() = definedExternally
    set(value) = definedExternally
  var onSortChange: ((dir: Number?) -> Unit)?
    get() = definedExternally
    set(value) = definedExternally
//    var children: Any?
//        get() = definedExternally
//        set(value) = definedExternally
}

//external object DataTableHeadCell {
//    @nativeInvoke
//    operator fun invoke(props: DataTableHeadCellProps /* DataTableHeadCellProps & RMWC. RProps */): Any
//    var displayName: String
//}

//external var DataTableHeadCell: Component<DataTableHeadCellProps, RState>
external class DataTableHeadCell: Component<DataTableHeadCellProps, RState> {
  override fun render()
}

external interface DataTableCellProps : SharedDataTableCellProps

//external var DataTableCell: Component<DataTableCellProps, RState>
external class DataTableCell: Component<DataTableCellProps, RState> {
  override fun render()
}

external interface SimpleDataTableProps : DataTableProps {
  var data: Array<Array<Any>>
  var headers: Array<Array<Any>>?
    get() = definedExternally
    set(value) = definedExternally
  var getRowProps: ((row: Array<Any>, index: Number, isHead: Boolean) -> Any)?
    get() = definedExternally
    set(value) = definedExternally
  var getCellProps: ((cell: Array<Any>, index: Number, isHead: Boolean) -> Any)?
    get() = definedExternally
    set(value) = definedExternally
}

external open class SimpleDataTable : Component<SimpleDataTableProps, RState> {
  override fun render(): Any
}
