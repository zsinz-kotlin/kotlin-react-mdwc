@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")

@file:JsModule("@rmwc/dialog")

package com.zsinz.kotlin.react.mdwc.types

import React.ReactNode
import react.Component
import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.RBuilder
import react.RProps
import react.RState

external interface DialogTitleProps : ComponentProps

@JsName("DialogTitle")
external open class DialogTitle: Component<DialogTitleProps /* DialogTitleProps & Any */, RState>{
  override fun render()
}

external interface DialogContentProps : ComponentProps

@JsName("DialogContent")
external open class DialogContent: Component<DialogContentProps /* DialogContentProps & Any */, RState>{
  override fun render()
}

external interface DialogActionsProps : ComponentProps{}

@JsName("DialogActions")
external open class  DialogActions: Component<DialogActionsProps /* DialogActionsProps & Any */, RState>{
  override fun render()
}

external interface DialogButtonProps : ButtonProps {
    var action: String?
        get() = definedExternally
        set(value) = definedExternally
    var isDefaultAction: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

external open class DialogButton : Component<DialogButtonProps /* DialogButtonProps & RMWC.ComponentProps */, RState> {
    override open fun render(): ReactNode

    companion object {
        var displayName: String
    }
}

//typealias DialogOnOpenEventT = CustomEvent<Any>

external interface DialogActionEvent {
    var action: String?
        get() = definedExternally
        set(value) = definedExternally
}

external interface MDCDialogAdapter {
  fun addClass(className: String)
  fun removeClass(className: String)
  fun hasClass(className: String): Boolean
  fun addBodyClass(className: String)
  fun removeBodyClass(className: String)
  fun eventTargetMatches(target: EventTarget?, selector: String): Boolean
  fun isContentScrollable(): Boolean
  fun areButtonsStacked(): Boolean
  fun getActionFromEvent(evt: Event): String?
  fun trapFocus(focusElement: HTMLElement?)
  fun releaseFocus()
  fun getInitialFocusEl(): HTMLElement?
  fun clickDefaultButton()
  fun reverseButtons()
  fun notifyOpening()
  fun notifyOpened()
  fun notifyClosing(action: String)
  fun notifyClosed(action: String)
}

external open class MDCDialogFoundation(adapter: Any? = definedExternally) : MDCFoundation<MDCDialogAdapter> {
  open var isOpen_: Any
  open var animationFrame_: Any
  open var animationTimer_: Any
  open var layoutFrame_: Any
  open var escapeKeyAction_: Any
  open var scrimClickAction_: Any
  open var autoStackButtons_: Any
  open var areButtonsStacked_: Any
  override fun init()
  override fun destroy()
  open fun open()
  open fun close(action: String? = definedExternally)
  open fun isOpen(): Boolean
  open fun getEscapeKeyAction(): String
  open fun setEscapeKeyAction(action: String)
  open fun getScrimClickAction(): String
  open fun setScrimClickAction(action: String)
  open fun getAutoStackButtons(): Boolean
  open fun setAutoStackButtons(autoStack: Boolean)
  open fun layout()
  open fun handleClick(evt: MouseEvent)
  open fun handleKeydown(evt: KeyboardEvent)
  open fun handleDocumentKeydown(evt: KeyboardEvent)
  open var layoutInternal_: Any
  open var handleAnimationTimerEnd_: Any
  open var runNextAnimationFrame_: Any
  open var detectStackedButtons_: Any
  open var detectScrollableContent_: Any

//    companion object {
//        var cssClasses: `T$0`
//        var strings: `T$1`
//        var numbers: `T$2`
//        var defaultAdapter: MDCDialogAdapter
//    }
}

//typealias DialogOnCloseEventT = CustomEvent<DialogActionEvent>

external interface DialogProps : ComponentProps {
    var open: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var onOpen: ((evt: CustomEvent<Any> ) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var onClose: ((evt: CustomEvent<DialogActionEvent>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var onStateChange: ((state: dynamic /* 'opening' | 'opened' | 'closing' | 'closed' */) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var preventOutsideDismiss: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("Dialog")
external open class Dialog(props: DialogProps) : FoundationComponent<MDCDialogFoundation, DialogProps, RState> {
    open var root: Any
    open var container: HTMLElement?
    open var content: HTMLElement?
    open var buttons: Array<HTMLElement>?
    open var defaultButton: HTMLElement?
    open var focusTrap: dynamic
    open var handleDocumentKeydown: (evt: KeyboardEvent) -> Unit
    open fun open()
    open fun close()
    override open fun componentDidMount()
    override open fun componentWillUnmount()
    open fun sync(props: DialogProps)
    override open fun getDefaultFoundation(): MDCDialogFoundation
    open fun handleClick(evt: MouseEvent /* React.MouseEvent & MouseEvent */)
    open fun handleKeydown(evt: KeyboardEvent /* React.KeyboardEvent & KeyboardEvent */)
    override open fun render(): ReactNode

//    companion object {
//        var displayName: String
//    }
}

//external interface SimpleDialogProps : DialogProps {
//    var title: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//    var header: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//    var body: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//    var footer: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//    var acceptLabel: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//    var cancelLabel: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//    var children: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//}

//external interface `T$0` {
//    var title: Nothing?
//        get() = definedExternally
//        set(value) = definedExternally
//    var header: Nothing?
//        get() = definedExternally
//        set(value) = definedExternally
//    var body: Nothing?
//        get() = definedExternally
//        set(value) = definedExternally
//    var footer: Nothing?
//        get() = definedExternally
//        set(value) = definedExternally
//    var acceptLabel: String
//    var cancelLabel: String
//    var open: Boolean
//    var children: Nothing?
//        get() = definedExternally
//        set(value) = definedExternally
//}
//
//external open class SimpleDialog : React.Component<SimpleDialogProps /* SimpleDialogProps & Any */> {
//    open fun render(): ReactNode
//
//    companion object {
//        var displayName: String
//        var defaultProps: `T$0`
//    }
//}
