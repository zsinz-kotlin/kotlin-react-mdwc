@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/drawer")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import react.Component
import react.RState
import com.zsinz.kotlin.react.mdwc.types.ComponentProps
import com.zsinz.kotlin.react.mdwc.types.CustomEvent

external interface DrawerHeaderProps : ComponentProps

@JsName("DrawerHeader")
external open class DrawerHeader: Component<DrawerHeaderProps /* DialogTitleProps & Any */, RState> {
  override fun render()
}

external interface DrawerTitleProps : ComponentProps

@JsName("DrawerTitle")
external open class DrawerTitle: Component<DrawerTitleProps /* DialogTitleProps & Any */, RState> {
  override fun render()
}

external interface DrawerSubtitleProps : ComponentProps

@JsName("DrawerSubtitle")
external open class DrawerSubtitle: Component<DrawerSubtitleProps /* DialogTitleProps & Any */, RState> {
  override fun render()
}

external interface DrawerContentProps : ComponentProps

@JsName("DrawerContent")
external open class DrawerContent: Component<DrawerContentProps /* DialogTitleProps & Any */, RState> {
  override fun render()
}

external interface DrawerAppContentProps : ComponentProps

@JsName("DrawerAppContent")
external open class DrawerAppContent: Component<DrawerAppContentProps /* DialogTitleProps & Any */, RState> {
  override fun render()
}

external interface DrawerProps : ComponentProps{
    var open: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var onClose: ((evt: CustomEvent<Any>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var onOpen: ((evt: CustomEvent<Any>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var dismissible: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var modal: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("DrawerRoot")
external open class DrawerRoot: Component<DrawerProps /* DialogTitleProps & Any */, RState> {
  override fun render()
}

@JsName("Drawer")
external open class Drawer: Component<DrawerProps /* DialogTitleProps & Any */, RState> {
  override fun render()
}
