@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RComponent
import react.RProps
import react.RState

external open class FoundationElement<Props : Any, ElementType>(onChange: () -> Unit) {
    open var _classes: Any
    open var _events: Any
    open var _style: Any
    open var _props: Any
    open var _ref: Any
    open var _onChange: (() -> Unit)?
    open fun onChange()
    open fun destroy()
    open fun addClass(className: String)
    open fun removeClass(className: String)
    open fun hasClass(className: String): Boolean
    open fun setProp(propName: Any, value: Any)
    open fun getProp(propName: Any): Any
    open fun removeProp(propName: Any)
    open fun props(propsToMerge: Json): Any
    open fun setStyle(propertyName: String, value: Number)
    open fun setStyle(propertyName: String, value: String)
    open fun setStyle(propertyName: String, value: Nothing?)
    open fun addEventListener(evtName: String, callback: SpecificEventListener<Any>)
    open fun removeEventListener(evtName: String, callback: SpecificEventListener<Any>)
    open fun setRef(el: Any)
    open var ref: ElementType?
}

typealias ExtractProps<TComponentOrTProps> = Any

external interface FoundationProps : ComponentProps

external interface FoundationState

external interface FoundationPropsT<P> : ComponentProps

external interface FoundationElementMap {
}

@Suppress("NOTHING_TO_INLINE")
inline operator fun FoundationElementMap.get(key: String): FoundationElement<Any, Any>? = asDynamic()[key]

@Suppress("NOTHING_TO_INLINE")
inline operator fun FoundationElementMap.set(key: String, value: FoundationElement<Any, Any>) {
    asDynamic()[key] = value
}

external open class FoundationComponent<Foundation : Any, P : RProps, S : RState>(props: Any) :
  Component<P, S /* S & FoundationState */> {
  override fun render(): dynamic

  open var foundation: Foundation
  open var elements: FoundationElementMap
  override open fun componentDidMount()
  open fun componentDidUpdate(prevProps: FoundationPropsT<P>)
  override open fun componentWillUnmount()
  open fun <ElementType : Any> createElement(elementName: String): FoundationElement<ExtractProps<ElementType>, ElementType>
  open fun update()
  open fun sync(props: Any, prevProps: Any? = definedExternally)
  open fun syncProp(prop: Any, prevProp: Any, callback: () -> Unit)
  open fun getDefaultFoundation(): Foundation
  open fun emit(evtType: String, evtData: Any, shouldBubble: Boolean? = definedExternally): CustomEvent<Any>

  companion object {
    var shouldDebounce: Boolean
  }
}

