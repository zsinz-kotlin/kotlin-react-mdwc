@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/grid")

package com.zsinz.kotlin.react.mdwc.types
import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RState
import com.zsinz.kotlin.react.mdwc.types.ComponentProps

external interface GridProps : ComponentProps {
    var fixedColumnWidth: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var align: dynamic /* 'left' | 'right' */
        get() = definedExternally
        set(value) = definedExternally
    var children: React.ReactNode?
        get() = definedExternally
        set(value) = definedExternally
}


@JsName("Grid")
external open class Grid: Component<GridProps, RState> {
  override fun render()
}

external interface GridCellProps : ComponentProps {
    var span: Number?
        get() = definedExternally
        set(value) = definedExternally
    var phone: Number?
        get() = definedExternally
        set(value) = definedExternally
    var tablet: Number?
        get() = definedExternally
        set(value) = definedExternally
    var desktop: Number?
        get() = definedExternally
        set(value) = definedExternally
    var order: Number?
        get() = definedExternally
        set(value) = definedExternally
    var align: dynamic /* 'top' | 'middle' | 'bottom' */
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("GridCell")
external open class GridCell: Component<GridCellProps, RState> {
  override fun render()
}

external interface GridInnerProps : ComponentProps

@JsName("GridInner")
external open class GridInner: Component<GridInnerProps, RState> {
  override fun render()
}
