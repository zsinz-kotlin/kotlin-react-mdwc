@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/icon-button")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
//import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RState

external interface IconButtonChange {
    var isOn: Boolean
}

external interface IconProps : ComponentProps {
  var icon: IconElementT
}

external interface IconOptions {
  var icon: IconElementT
  var strategy: dynamic /* 'auto' | 'ligature' | 'className' | 'url' | 'component' | 'custom' */
    get() = definedExternally
    set(value) = definedExternally
  var prefix: String?
    get() = definedExternally
    set(value) = definedExternally
  var basename: String?
    get() = definedExternally
    set(value) = definedExternally
  var render: ((props: IconProps) -> React.ReactNode)?
    get() = definedExternally
    set(value) = definedExternally
  var size: dynamic /* 'xsmall' | 'small' | 'medium' | 'large' | 'xlarge' */
    get() = definedExternally
    set(value) = definedExternally
//  @nativeGetter
//  operator fun get(key: String): Any?
//  @nativeSetter
//  operator fun set(key: String, value: Any)
}

external interface IconButtonProps : WithRippleProps {
    var checked: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var onChange: ((evt: CustomEvent<IconButtonChange>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var disabled: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var icon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var onIcon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("IconButton")
external open class IconButton: Component<IconButtonProps, RState> {
  override fun render()
}
