@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/menu")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.Component
import react.RProps
import react.RState

//typealias MenuSurfaceOnOpenEventT = CustomEvent<Any>
//
//typealias MenuSurfaceOnCloseEventT = CustomEvent<Any>

external interface MenuSurfaceProps : ComponentProps{
  var open: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var fixed: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var hoistToBody: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var anchorCorner: dynamic /* 'bottomEnd' | 'bottomLeft' | 'bottomRight' | 'bottomStart' | 'topEnd' | 'topLeft' | 'topRight' | 'topStart' */
    get() = definedExternally
    set(value) = definedExternally
  var onOpen: ((evt: CustomEvent<Any>) -> Unit)?
    get() = definedExternally
    set(value) = definedExternally
  var onClose: ((evt: CustomEvent<Any>) -> Unit)?
    get() = definedExternally
    set(value) = definedExternally
  var children: dynamic /*React.ReactNode?*/
    get() = definedExternally
    set(value) = definedExternally
}

external interface MDCMenuSurfaceAdapter {
}

external open class MDCMenuSurfaceFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) :
  MDCFoundation<MDCMenuSurfaceAdapter> {
}

@JsName("MenuSurface")
external open class MenuSurface(props: MenuSurfaceProps) : FoundationComponent<MDCMenuSurfaceFoundation, MenuSurfaceProps, RState> {
}


@JsName("MenuSurfaceAnchor")
external open class MenuSurfaceAnchor(props: RProps) :
  Component<RProps, RState> {
  override fun render(): dynamic
}


external interface MenuSelected {
  var item: HTMLElement
  var index: Number
}

//typealias MenuOnSelectEventT = CustomEvent<MenuSelected>

external interface MenuProps : MenuSurfaceProps {
    var onSelect: ((evt: CustomEvent<Any>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var focusOnOpen: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

external interface MenuItemsProps : ListProps

//external var MenuItems: MenuItemsProps
@JsName("MenuItems")
external open class MenuItems(props: MenuItemsProps) :
  Component<MenuItemsProps, RState> {
  override fun render(): dynamic
}


external interface MenuItemProps : ListItemProps

//external var MenuItem: MenuItemProps
@JsName("MenuItem")
external open class MenuItem(props: MenuItemProps) :
  Component<MenuItemsProps, RState> {
  override fun render(): dynamic
}

external interface MDCMenuAdapter {
}

external open class MDCMenuFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) :
  MDCFoundation<MDCMenuAdapter> {
}

@JsName("Menu")
external open class Menu(props: MenuProps) : FoundationComponent<MDCMenuFoundation, MenuProps, RState> {
//    open var list: List?
//    open var menuSurface: MenuSurface?
//    open var items: Array<HTMLLIElement>
//    open fun hoistMenuToBody()
//    open fun setAnchorCorner(corner: Corner)
//    open fun setAnchorElement(element: HTMLElement)
//    open fun getDefaultFoundation(): MDCMenuFoundation
//    open fun handleClick(evt: React.MouseEvent)
//    open fun handleKeydown(evt: React.KeyboardEvent /* React.KeyboardEvent & KeyboardEvent */)
//    open fun handleOpen(evt: MenuSurfaceOnOpenEventT)
//    open fun render(): ReactNode
//
//    companion object {
//        var displayName: String
//        var defaultProps: `T$0`
//    }
}

//external interface SimpleMenuProps : MenuProps {
//    var handle: React.ReactElement<Any>
//    var rootProps: Any?
//        get() = definedExternally
//        set(value) = definedExternally
//    var children: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//}
//
//external interface SimpleMenuSurfaceProps : MenuSurfaceProps {
//    var handle: React.ReactElement<Any>
//    var rootProps: Any?
//        get() = definedExternally
//        set(value) = definedExternally
//    var children: React.ReactNode?
//        get() = definedExternally
//        set(value) = definedExternally
//}
//
//external interface SimpleMenuState {
//    var open: Boolean
//}
//
//external var SimpleMenu: React.ComponentType<SimpleMenuProps>
//
//external var SimpleMenuSurface: React.ComponentType<SimpleMenuSurfaceProps>
