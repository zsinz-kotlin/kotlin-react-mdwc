@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/select")

package com.zsinz.kotlin.react.mdwc.types

import React.ReactNode
import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.RProps
import react.RState

external interface FormattedOption  {
    open var label: String?
    open var value: String?
//        get() = definedExternally
//        set(value) = definedExternally
    open var options: Array<FormattedOption>?
//        get() = definedExternally
//        set(value) = definedExternally
}

external interface FormattedOptionMap {
//    @nativeGetter
//    operator fun get(value: String): String?
//    @nativeSetter
//    operator fun set(key: String, value: String)
}

external interface SelectProps : ComponentProps {
  var value: String?
    get() = definedExternally
    set(value) = definedExternally
  var helpText: dynamic /* React.ReactNode | SelectHelperTextProps */
    get() = definedExternally
    set(value) = definedExternally
  var options: dynamic /* Array<FormattedOption> | Array<String> | FormattedOptionMap */
    get() = definedExternally
    set(value) = definedExternally
  var label: String?
    get() = definedExternally
    set(value) = definedExternally
  var placeholder: String?
    get() = definedExternally
    set(value) = definedExternally
  var outlined: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var invalid: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var disabled: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var required: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var enhanced: dynamic /* Boolean | MenuProps */
    get() = definedExternally
    set(value) = definedExternally
  var rootProps: Any?
    get() = definedExternally
    set(value) = definedExternally
  var inputRef: ((ref: HTMLSelectElement?) -> Unit)?
    get() = definedExternally
    set(value) = definedExternally
  var icon: dynamic /* IconElementT | IconOptions */
    get() = definedExternally
    set(value) = definedExternally

  var onChange: dynamic
    get() = definedExternally
    set(value) = definedExternally

  var defaultValue: dynamic
    get() = definedExternally
    set(value) = definedExternally

}

external interface DeprecatedSelectProps {
    var withLeadingIcon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
}

external interface SelectState : RState {
  var selectedIndex: Number
  var selectedTextContent: String
  var menuOpen: Boolean
}

external interface NativeSelectAdapterMethod {
    var getValue: () -> String
    var setValue: (value: String) -> String?
    var openMenu: () -> Unit
    var closeMenu: () -> Unit
    var isMenuOpen: () -> Boolean
    var setSelectedIndex: (index: Number) -> Unit
    var setDisabled: (isDisabled: Boolean) -> Boolean?
    var setValid: (isValid: Boolean) -> Unit
    var checkValidity: () -> Boolean
}

external interface EnhancedSelectAdapterMethods {
    var getValue: () -> String
    var setValue: (value: String) -> Unit
    var openMenu: () -> Unit
    var closeMenu: () -> Unit
    var isMenuOpen: () -> Boolean
    var setSelectedIndex: (index: Number) -> Unit
    var setDisabled: (isDisabled: Boolean) -> Unit
    var checkValidity: () -> Boolean
    var setValid: (isValid: Boolean) -> Unit
}

external interface CommonAdapterMethods {
    var addClass: (className: String) -> Unit
    var removeClass: (className: String) -> Unit
    var hasClass: (className: String) -> Boolean
    var isRtl: () -> Boolean?
    var setRippleCenter: (normalizedX: Number) -> Unit
    var activateBottomLine: () -> Unit
    var deactivateBottomLine: () -> Unit
    var notifyChange: (value: Any) -> Unit
}

external interface OutlineAdapterMethods {
    var hasOutline: () -> Boolean
    var notchOutline: (labelWidth: Number) -> Unit
    var closeOutline: () -> Unit
}

external interface LabelAdapterMethods {
    var floatLabel: (shouldFloat: Boolean) -> Unit
    var getLabelWidth: () -> Number
}

external interface MDCSelectAdapter {
}

external open class MDCSelectFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) : MDCFoundation<MDCSelectAdapter> {
}

//external interface FoundationMap {
//    var leadingIcon: MDCSelectIconFoundation?
//        get() = definedExternally
//        set(value) = definedExternally
//}

@JsName("SelectBase")
external open class SelectBase(props: SelectProps) : FoundationComponent<MDCSelectFoundation, SelectProps /* SelectProps & DeprecatedSelectProps */, SelectState> {
    open var root: Any
    open var lineRipple: Any
    open var outline: Any
    open var label: Any
    open var id: String
    open var nativeControl: HTMLSelectElement?
    open var selectedText: HTMLElement?
    open var menuElement: HTMLElement?
    open var menu: Any? // TODO : REplace with menu type: Menu?
    open var hiddenInput_: HTMLInputElement?
    open var leadingIcon_: dynamic
    open var trailingIcon_: HTMLElement?
    override open var state: SelectState
    override open fun componentDidMount()
    override open fun getDefaultFoundation(): MDCSelectFoundation
    open fun getNativeSelectAdapterMethods_(): NativeSelectAdapterMethod
    open fun getEnhancedSelectAdapterMethods_(): EnhancedSelectAdapterMethods
    open fun getCommonAdapterMethods_(): CommonAdapterMethods
    open fun getOutlineAdapterMethods_(): OutlineAdapterMethods
    open fun getLabelAdapterMethods_(): LabelAdapterMethods
    open fun getFoundationMap_(): dynamic /*FoundationMap*/
    open fun sync(props: SelectProps, prevProps: SelectProps)
    open var value: String
    open fun handleChange(evt: Any)
    open fun handleFocus(evt: Any)
    open fun handleBlur(evt: Any)
    open fun handleClick(evt: Any)
    open fun handleKeydown(evt: Any)
    open fun handleMenuSelected(evt: CustomEvent<MenuSelected> /* CustomEvent<`T$8`> & React.SyntheticEvent<EventTarget> */)
    open fun handleMenuOpened()
    open fun handleMenuClosed()
    open fun renderIcon(iconNode: Any, leadOrTrail: String /* 'leadingIcon_' */): Any
//    open fun renderIcon(iconNode: Any, leadOrTrail: String /* 'trailingIcon_' */): Any
    open fun renderHelpText(): ReactNode?
    override open fun render(): ReactNode
}

//external open class SelectIcon : FoundationComponent<MDCSelectIconFoundation, IconProps, RState> {
//    open var root: Any
//    override open fun getDefaultFoundation(): MDCSelectIconFoundation
//    override open fun render(): ReactNode
//
//    companion object {
//        var displayName: String
//    }
//}
//
//external interface SelectHelperTextProps {
//    var persistent: Boolean?
//        get() = definedExternally
//        set(value) = definedExternally
//    var validationMsg: Boolean?
//        get() = definedExternally
//        set(value) = definedExternally
//}

//external var SelectHelperText: React.ComponentType<SelectHelperTextProps /* SelectHelperTextProps & Any */>

//@JsName("Select")
//external class SelectComponent(selectProps: SelectProps): SelectBase {
//
//}

//external object Select {
//    @nativeInvoke
//    operator fun invoke(__0: SelectProps /* SelectProps & RMWC.ComponentProps */): ReactNode
//    var displayName: String
//}
