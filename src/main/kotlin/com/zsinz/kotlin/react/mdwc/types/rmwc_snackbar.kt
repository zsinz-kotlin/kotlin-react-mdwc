@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS")

@file:JsModule("@rmwc/snackbar")

package com.zsinz.kotlin.react.mdwc.types
import kotlin.js.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import react.Component
import react.RClass
import react.RState

external interface SnackbarOnOpenEvent

external interface SnackbarOnCloseEvent {
    var reason: String?
        get() = definedExternally
        set(value) = definedExternally
}

external interface SnackbarProps : ComponentProps{
    var open: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var onOpen: ((evt: CustomEvent<SnackbarOnOpenEvent>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var onClose: ((evt: CustomEvent<SnackbarOnCloseEvent>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var message: Any?
        get() = definedExternally
        set(value) = definedExternally
    var action: dynamic /* React.ReactNode? | Array<React.ReactNode>? */
        get() = definedExternally
        set(value) = definedExternally
    var timeout: Number?
        get() = definedExternally
        set(value) = definedExternally
    var stacked: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var leading: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var dismissIcon: dynamic /* Boolean? | String? */
        get() = definedExternally
        set(value) = definedExternally
    var dismissesOnAction: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var icon: Any?
        get() = definedExternally
        set(value) = definedExternally
}

external interface DeprecatedSnackbarProps {
    var show: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var onShow: ((evt: Event) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var onHide: ((evt: Event) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var alignStart: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var multiline: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var actionOnBottom: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var actionHandler: (() -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var actionText: Any?
        get() = definedExternally
        set(value) = definedExternally
}

external interface SnackbarActionProps : ButtonProps {
    var action: String?
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("SnackbarAction")
external val SnackbarAction: RClass<SnackbarActionProps>

external interface MDCSnackbarAdapter

open external class MDCSnackbarFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) : MDCFoundation<MDCSnackbarAdapter>

@JsName("Snackbar")
open external class Snackbar(props: SnackbarProps) : FoundationComponent<MDCSnackbarFoundation, SnackbarProps, RState> {
    open var root: Any
    open var isShowing_: Boolean
    open var labelEl: HTMLElement?
    open var show: Any
    open var announce: Any
    override fun getDefaultFoundation(): MDCSnackbarFoundation
    open fun sync(props: SnackbarProps, prevProps: SnackbarProps)
    open fun getPropsWithDeprecations(props: SnackbarProps): Any
    open fun handleKeyDown(evt: MouseEvent /* React.KeyboardEvent & KeyboardEvent */)
}