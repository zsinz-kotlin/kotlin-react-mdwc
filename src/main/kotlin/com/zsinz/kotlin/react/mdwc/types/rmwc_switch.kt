@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/switch")

package com.zsinz.kotlin.react.mdwc.types
import kotlin.js.*
import react.RState

external interface ToggleableFoundationProps : ComponentProps{
  var id: String?
    get() = definedExternally
    set(value) = definedExternally
  var disabled: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var checked: Boolean?
    get() = definedExternally
    set(value) = definedExternally
  var value: dynamic /* String | Number | Array<String> */
    get() = definedExternally
    set(value) = definedExternally
  var label: dynamic
    get() = definedExternally
    set(value) = definedExternally
  var rootProps: dynamic
    get() = definedExternally
    set(value) = definedExternally
  var inputRef: dynamic /* React.MutableRefObject<HTMLInputElement?> | (ref: HTMLInputElement?) -> Unit */
    get() = definedExternally
    set(value) = definedExternally
}

//open external class ToggleableFoundationComponent<Foundation : Any, P : ToggleableFoundationProps, S : Any> :  FoundationComponent<Foundation, RProps /* P & FoundationProps */, RState> {
//  open var generatedId: String
//  open fun renderToggle(toggle: React.ReactNode): Any?
//}

external interface SwitchProps : WithRippleProps, ToggleableFoundationProps

external interface MDCSwitchAdapter {
}

open external class MDCSwitchFoundation(adapter: Any? = definedExternally) : MDCFoundation<MDCSwitchAdapter> {
}

@JsName("Switch")
open external class Switch : FoundationComponent<MDCSwitchFoundation, SwitchProps, RState>{
  open var root: Any
  open var nativeControl: Any
  open override fun componentDidMount()
  open fun handleChange(evt: Any)
  open fun sync(props: SwitchProps, prevProps: SwitchProps? = definedExternally)
  open override fun render(): Any?

  companion object {
    var displayName: String
  }
}
