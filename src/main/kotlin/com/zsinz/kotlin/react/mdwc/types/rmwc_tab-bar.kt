@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/tabs")

package com.zsinz.kotlin.react.mdwc.types

import React.ReactNode
import kotlin.js.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import react.RProps
import react.RState

external interface TabBarState {
    var index: Number
}

//typealias TabBarOnActivateEvent = CustomEvent<TabBarState>

external interface TabBarProps : ComponentProps {
    var onActivate: ((evt: CustomEvent<TabBarState>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var activeTabIndex: Number?
        get() = definedExternally
        set(value) = definedExternally
    var indicatorTransition: dynamic /* 'slide' | 'fade' */
        get() = definedExternally
        set(value) = definedExternally
}

//external var TabBarRoot: React.ComponentType<TabBarProps /* TabBarProps & Any */>

external interface MDCTabBarAdapter {

}

external open class MDCTabBarFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) : MDCFoundation<MDCTabBarAdapter> {

}

@JsName("TabBar")
external open class TabBar(props: TabBarProps) : FoundationComponent<MDCTabBarFoundation, TabBarProps, RState> {
    open var root: Any
    open var currentActiveTabIndex: Any
//    open var tabScroller: TabScroller?
//    open var contextApi: TabBarContextT
    open var tabList: Array<Any>

    override open fun componentDidMount()
    open fun activateTab(index: Number)
    override fun getDefaultFoundation(): MDCTabBarFoundation
    open fun sync(props: TabBarProps, prevProps: TabBarProps)
    open fun getTabElements(): Array<Element>?
    open fun handleTabInteraction(evt: CustomEvent<TabChangeEvent>)
    open fun handleKeyDown(evt: KeyboardEvent)
    override fun render(): ReactNode

    companion object {
        var displayName: String
    }
}
