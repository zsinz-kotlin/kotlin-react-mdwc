@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/tabs")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.RProps
import react.RState

external interface TabChangeEvent {
    var tabId: String
}

//typealias MDCTabInteractionEvent = CustomEvent<TabChangeEvent>

external interface TabProps : ComponentProps {
    var label: Any?
        get() = definedExternally
        set(value) = definedExternally
    var children: React.ReactNode?
        get() = definedExternally
        set(value) = definedExternally
    var icon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var iconIndicator: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var stacked: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var restrictIndicator: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var onInteraction: ((evt: CustomEvent<TabChangeEvent>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
}

external interface MDCTabAdapter {
}

external open class MDCTabFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) : MDCFoundation<MDCTabAdapter> {
}

@JsName("Tab")
external open class Tab(props: TabProps) : FoundationComponent<MDCTabFoundation, TabProps /* SelectProps & DeprecatedSelectProps */, RState> {
}

