@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/textfield")

package com.zsinz.kotlin.react.mdwc.types

import React.ReactNode
import kotlinx.html.InputType
import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.RProps
import react.RState

external interface TextFieldProps : ComponentProps {
    var value: dynamic /* String | Number */
        get() = definedExternally
        set(value) = definedExternally
    var helpText: dynamic /* React.ReactNode | TextFieldHelperTextProps */
        get() = definedExternally
        set(value) = definedExternally
    var characterCount: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var invalid: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var disabled: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var required: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var pattern: String?
        get() = definedExternally
        set(value) = definedExternally
    var outlined: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var label: dynamic /* React.ReactNode? */
        get() = definedExternally
        set(value) = definedExternally
    var textarea: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var fullwidth: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var icon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var trailingIcon: dynamic /* IconElementT | IconOptions */
        get() = definedExternally
        set(value) = definedExternally
    var rootProps: Any?
        get() = definedExternally
        set(value) = definedExternally
    var inputRef: dynamic /* React.MutableRefObject<dynamic /* HTMLInputElement | HTMLTextAreaElement | Nothing? */> | (ref: dynamic /* HTMLInputElement | HTMLTextAreaElement | Nothing? */) -> Unit */
        get() = definedExternally
        set(value) = definedExternally
    var type: dynamic
        get() = definedExternally
        set(value) = definedExternally

    var onChange: dynamic
        get() = definedExternally
        set(value) = definedExternally

}


external interface TextFieldState : RState {
  var content: String
}

external interface MDCTextFieldAdapter {

}

external open class MDCTextFieldFoundation(adapter: Any? = definedExternally, foundationMap: Any? = definedExternally) : MDCFoundation<MDCTextFieldAdapter> {

}

@JsName("TextField")
external open class TextField(props: TextFieldProps) : FoundationComponent<MDCTextFieldFoundation, TextFieldProps, TextFieldState> {

  override open fun render(): ReactNode

  companion object {
    var displayName: String
  }
}
