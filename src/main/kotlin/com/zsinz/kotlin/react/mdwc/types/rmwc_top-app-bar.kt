@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
@file:JsModule("@rmwc/top-app-bar")

package com.zsinz.kotlin.react.mdwc.types

import org.w3c.dom.Element
import kotlin.js.*
import react.Component
import react.RState
import com.zsinz.kotlin.react.mdwc.types.ComponentProps
import com.zsinz.kotlin.react.mdwc.types.CustomEvent
import com.zsinz.kotlin.react.mdwc.types.IconButtonProps

external interface TopAppBarProps: ComponentProps {
    var onNav: ((evt: CustomEvent<Any>) -> Unit)?
        get() = definedExternally
        set(value) = definedExternally
    var fixed: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var prominent: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var short: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var shortCollapsed: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var dense: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var scrollTarget: Element?
        get() = definedExternally
        set(value) = definedExternally
}

external interface TopAppBarRowProps: ComponentProps

@JsName("TopAppBarRow")
external open class TopAppBarRow: Component<TopAppBarRowProps, RState> {
  override fun render()
}

external interface TopAppBarSectionProps: ComponentProps {
    var alignStart: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var alignEnd: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("TopAppBarSection")
external open class TopAppBarSection: Component<TopAppBarSectionProps, RState> {
  override fun render()
}

external interface TopAppBarNavigationIconProps : IconButtonProps

@JsName("TopAppBarNavigationIcon")
external open class TopAppBarNavigationIcon: Component<TopAppBarNavigationIconProps, RState> {
  override fun render()
}

external interface TopAppBarActionItemProps : IconButtonProps

@JsName("TopAppBarActionItem")
external open class TopAppBarActionItem: Component<TopAppBarActionItemProps, RState> {
  override fun render()
}

external interface TopAppBarTitleProps: ComponentProps

@JsName("TopAppBarTitle")
external open class TopAppBarTitle: Component<TopAppBarTitleProps, RState> {
  override fun render()
}

external interface TopAppBarFixedAdjustProps: ComponentProps {
    var dense: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var prominent: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var denseProminent: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var short: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

@JsName("TopAppBarFixedAdjust")
external open class TopAppBarFixedAdjust: Component<TopAppBarFixedAdjustProps, RState> {
  override fun render()
}

@JsName("TopAppBar")
external open class TopAppBar: Component<TopAppBarProps, RState> {
  override fun render()
}
