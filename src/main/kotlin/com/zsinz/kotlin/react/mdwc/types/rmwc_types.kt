@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")

package com.zsinz.kotlin.react.mdwc.types

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.RProps

external interface CustomEvent<E> : Event

external interface ComponentProps: RProps {

  var tag: dynamic /* String | React.ComponentType<Any> */
    get() = definedExternally
    set(value) = definedExternally
  var theme: dynamic /* 'primary' | 'secondary' | 'background' | 'surface' | 'error' | 'primaryBg' | 'secondaryBg' | 'onPrimary' | 'onSecondary' | 'onSurface' | 'onError' | 'textPrimaryOnBackground' | 'textSecondaryOnBackground' | 'textHintOnBackground' | 'textDisabledOnBackground' | 'textIconOnBackground' | 'textPrimaryOnLight' | 'textSecondaryOnLight' | 'textHintOnLight' | 'textDisabledOnLight' | 'textIconOnLight' | 'textPrimaryOnDark' | 'textSecondaryOnDark' | 'textHintOnDark' | 'textDisabledOnDark' | 'textIconOnDark' | Array<dynamic /* 'primary' | 'secondary' | 'background' | 'surface' | 'error' | 'primaryBg' | 'secondaryBg' | 'onPrimary' | 'onSecondary' | 'onSurface' | 'onError' | 'textPrimaryOnBackground' | 'textSecondaryOnBackground' | 'textHintOnBackground' | 'textDisabledOnBackground' | 'textIconOnBackground' | 'textPrimaryOnLight' | 'textSecondaryOnLight' | 'textHintOnLight' | 'textDisabledOnLight' | 'textIconOnLight' | 'textPrimaryOnDark' | 'textSecondaryOnDark' | 'textHintOnDark' | 'textDisabledOnDark' | 'textIconOnDark' */> */
    get() = definedExternally
    set(value) = definedExternally


  var className: String?

  var style: dynamic
    get() = definedExternally
    set(value) = definedExternally


  /** "collapse" an element onto its children */
  var wrap: Boolean?

//  /** TODO generic events must be removed from this interface */
//  var onClick: (Event) -> Unit
}


external interface RipplePropT {
    var accent: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var surface: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var unbounded: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

external interface DeprecatedRippleProps: ComponentProps {
    var accent: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var surface: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var unbounded: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

external interface WithRippleProps : DeprecatedRippleProps {
  var ripple: dynamic /* Boolean | RipplePropT */
    get() = definedExternally
    set(value) = definedExternally
  var onClick: ((Event) -> Unit)?
}

//external interface ComponentProps : React.HTMLProps<Any> {
//    var tag: dynamic /* String | React.ComponentType<Any> */
//        get() = definedExternally
//        set(value) = definedExternally
//    var theme: dynamic /* 'primary' | 'secondary' | 'background' | 'surface' | 'error' | 'primaryBg' | 'secondaryBg' | 'onPrimary' | 'onSecondary' | 'onSurface' | 'onError' | 'textPrimaryOnBackground' | 'textSecondaryOnBackground' | 'textHintOnBackground' | 'textDisabledOnBackground' | 'textIconOnBackground' | 'textPrimaryOnLight' | 'textSecondaryOnLight' | 'textHintOnLight' | 'textDisabledOnLight' | 'textIconOnLight' | 'textPrimaryOnDark' | 'textSecondaryOnDark' | 'textHintOnDark' | 'textDisabledOnDark' | 'textIconOnDark' | Array<dynamic /* 'primary' | 'secondary' | 'background' | 'surface' | 'error' | 'primaryBg' | 'secondaryBg' | 'onPrimary' | 'onSecondary' | 'onSurface' | 'onError' | 'textPrimaryOnBackground' | 'textSecondaryOnBackground' | 'textHintOnBackground' | 'textDisabledOnBackground' | 'textIconOnBackground' | 'textPrimaryOnLight' | 'textSecondaryOnLight' | 'textHintOnLight' | 'textDisabledOnLight' | 'textIconOnLight' | 'textPrimaryOnDark' | 'textSecondaryOnDark' | 'textHintOnDark' | 'textDisabledOnDark' | 'textIconOnDark' */> */
//        get() = definedExternally
//        set(value) = definedExternally
//}

typealias IconElementT = React.ReactNode
